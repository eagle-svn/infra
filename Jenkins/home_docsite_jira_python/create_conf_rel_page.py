from atlassian import Confluence
import requests
import re
from cryptography.fernet import Fernet
import logging

logging.basicConfig(filename='/tmp/confluence_page.log', filemode='w',
                    format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)


key_file = 'jira.key'
with open(key_file, 'rb') as jk_r:
    jk_k = jk_r.read()

conf_eny_pass = b'gAAAAABdfYpy7c1AssXPTqXmK8xYzMoOmWVxoISRKceF9tP9FEzFKpKprcq6gT8K4hTptmRdVJv1RYFlmNDNpDbkMDH_8reDKg=='

cryp_obj = Fernet(jk_k)
conf_url = 'http://etinf03.eagletechva.com/confluence'
conf_user = 'pavank'
conf_pass = cryp_obj.decrypt(conf_eny_pass).decode('utf-8')

confluence = Confluence(
    url=conf_url,
    username=conf_user,
    password=conf_pass)

space = 'BHSIS'


def get_or_create_parent_id(component, fix_version):

    page_title = fix_version
    print("page title on top", page_title)
    logging.info('Component Name: '.format(component))
    logging.info('Release Version'.format(fix_version))
    logging.info('Page Title: '.format(page_title))

    if confluence.page_exists(space, page_title):
        parent_id = confluence.get_page_id(space, page_title)
        logging.info('Parent Page ID: '.format(parent_id))
        return parent_id
    else:
        component_page_tile = component + ' ' + 'Project Releases'
        component_page_id = confluence.get_page_id(space, component_page_tile)
        logging.info('Component Page Title: '.format(component_page_tile))
        logging.info('Component Page ID: '.format(component_page_id))
        page_body = '<p>Auto Created by Jenkins Deployment</p>'
        confluence.update_or_create(component_page_id, page_title, page_body, representation='storage')
        if confluence.page_exists(space, page_title):
            parent_id = confluence.get_page_id(space, page_title)
            logging.info('Parent Page ID from else: '.format(parent_id))
            return parent_id


def create_or_update_release_sheet(parent_id, component, fix_version, jira_tickets_table):
    page_title = '{0}_release_sheet'.format(fix_version)

    if confluence.page_exists(space, page_title):
        print('Page exist')
        logging.info('Page exist {}, {} '.format(space, page_title))
        page_id = confluence.get_page_id(space, page_title)
        logging.info('Page ID {}, {}, {}'.format(space, page_title, page_id))
        print(page_id)
        url = conf_url + '/rest/api/content/'+page_id+'?expand=body.storage'
        username = conf_user
        password = conf_pass
        req = requests.get(url, auth=(username, password))
        page_old_data_txt = (req.json())
        page_old_body = page_old_data_txt['body']['storage']['value']
        pattern = re.compile('<p />')
        page_old_body_html = re.sub(pattern, '', page_old_body)

        page_jira_table_html = "{0}{1}".format(page_old_body_html, jira_tickets_table)
        updated_page_body = '{0}'.format(page_jira_table_html)
        logging.info('Updated page_body '.format(updated_page_body))
        confluence.update_or_create(parent_id, page_title, updated_page_body, representation='storage')

    else:
        print('Page does not exist')
        logging.info('Confluence Page does not exist {}: '.format(page_title))

        with open('conf_rel.html', 'r') as fh:
            rel_html = fh.read()
        rel_html = rel_html.format(component)
        page_jira_table_html = "{0}".format(jira_tickets_table)
        page_body = rel_html + page_jira_table_html
        logging.info('page_body '.format(page_body))
        confluence.update_or_create(parent_id, page_title, page_body, representation='storage')




