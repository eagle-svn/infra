#!/bin/bash
##Created by Pavan Kumar, this script download the package and then start the application.

APP_DIR_PATH=/local/apps/supp_play_app

APP_NAME=supp_play_app

play clean $APP_DIR_PATH

play deps --sync  $APP_DIR_PATH

play start  $APP_DIR_PATH

while [[ $(ps -aef |grep play | grep java |grep ${APP_NAME} | grep -v grep) ]]
#while true
do
sleep 1
done
