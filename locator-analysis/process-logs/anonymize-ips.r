args = commandArgs(trailingOnly=TRUE)
options(width=300)
# REQUIRE FILENAME INPUTS
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).\n", call.=FALSE)
}

# GET THE FILES AND READ THEM IN
datalist = lapply(args, FUN=read.table, header=FALSE)

# ASSIGN DATA TO DATAFRAME
df = do.call("rbind", datalist) 

if (is.null(df)) {
  stop("No data was found.");
}

# NAME THE COLUMNS
names(df)[1] <- "IP_ADDRESS"

df$IP_ADDRESS <- gsub("\\.", "", df$IP_ADDRESS)
df$IP_ADDRESS <- as.numeric(df$IP_ADDRESS) * 8
df$IP_ADDRESS <- as.numeric(df$IP_ADDRESS) / 7
df$IP_ADDRESS <- gsub("\\.", "", df$IP_ADDRESS)
df$IP_ADDRESS <- gsub("8", "4", df$IP_ADDRESS)
df$IP_ADDRESS <- gsub("1", "0", df$IP_ADDRESS)
df$IP_ADDRESS <- gsub("3", "1", df$IP_ADDRESS)
df$IP_ADDRESS <- gsub("5", "2", df$IP_ADDRESS)
df$IP_ADDRESS <- gsub("6", "9", df$IP_ADDRESS)

write.table(df, "anon-ip.log", row.names=FALSE, col.names=FALSE, sep=" ")
