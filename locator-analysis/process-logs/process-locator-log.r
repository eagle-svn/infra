library(dplyr)

args = commandArgs(trailingOnly=TRUE)
options(width=300)
# REQUIRE FILENAME INPUTS
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).\n", call.=FALSE)
}

# GET THE FILES AND READ THEM IN
datalist = lapply(args, FUN=read.table, header=FALSE)

# ASSIGN DATA TO DATAFRAME
df = do.call("rbind", datalist) 

if (is.null(df)) {
  stop("No data was found.");
}

# NAME THE COLUMNS
names(df)[1] <- "IP_ADDRESS"
names(df)[4] <- "ACCESS_DATE"
names(df)[5] <- "TIMEZONE"
names(df)[6] <- "GET_URL"
names(df)[7] <- "RET_CODE"
names(df)[8] <- "SIZE"
names(df)[9] <- "REFERRAL"
names(df)[10] <- "AGENT"

# SET ACCESS DATE TO PRETTY FORMAT
df$ACCESS_DATE <- paste(df$ACCESS_DATE, df$TIMEZONE) %>% strptime(format = '[%d/%b/%Y:%H:%M:%S %z]', tz="America/New_York") %>% as.POSIXct
# ADD DATE AND HOUR COLUMNS
hourly_df <- df %>% group_by(DATE = format(df$ACCESS_DATE, "%Y-%m-%d"), HOUR = format(df$ACCESS_DATE,"%H"))

########################################
##### HITS PER PLATFORM DATA ###########
########################################
# CREATE PLATFORM DF
platform_hourly_count <- hourly_df
# ADD PLATFORM COLUMN
platform_hourly_count$VALUE <- ifelse(grepl("bot|crawl",x = platform_hourly_count$AGENT,ignore.case = TRUE), "Bot/Other",
                      ifelse(grepl("Android|Mobile|iP(hone,ad,od),",x = platform_hourly_count$AGENT,ignore.case = TRUE),"Mobile",
                      no =  ifelse(grepl("Windows Nt|Mac|Linux|CrOS|Darwin",x = platform_hourly_count$AGENT, ignore.case = TRUE),"Desktop","Bot/Other")))

# GET HITS PER PLATFORM PER HOUR
platform_hourly_count <- platform_hourly_count %>% group_by(DATE, HOUR, VALUE) %>% summarise(HITS = n())
platform_hourly_count <- platform_hourly_count[order(platform_hourly_count$DATE, platform_hourly_count$HOUR, decreasing = FALSE),]
platform_hourly_count$CATEGORY <- "PLATFORM"
platform_hourly_count <- platform_hourly_count[,c(1,2,5,3,4)]


########################################
##### REFERRALS DATA ###################
########################################
# GET REFERRALS PER REFERRAL_URL PER HOUR
pages_hourly_count <- hourly_df %>% group_by(LINK = GET_URL, REFERRAL, DATE, HOUR)
pages_hourly_count$URL <- paste(pages_hourly_count$REFERRAL,substring(pages_hourly_count$LINK,first = regexpr(pattern = "/",text = pages_hourly_count$LINK)+1))
# REMOVE LINK COLUMN
condensed_hourly_count <- pages_hourly_count %>% group_by(REFERRAL, DATE, HOUR) %>% summarise(HITS = n())
# REMOVE RECORDS WITH LESS THAN 50 HITS AND WITH BLANK REFERRAL TEXT
condensed_hourly_count <- condensed_hourly_count[(condensed_hourly_count$HITS > 50) & (condensed_hourly_count$REFERRAL != "-"),]

# ORDER THE RECORDS BY DATE AND HOUR
condensed_hourly_count <- condensed_hourly_count[order(condensed_hourly_count$DATE, condensed_hourly_count$HOUR, decreasing = FALSE),]

# REORDER THE COLUMNS
condensed_hourly_count$CATEGORY <- "REFERRALS"
condensed_hourly_count <- condensed_hourly_count[,c(2,3,5,1,4)]


########################################
##### TOP PAGES DATA ###################
########################################
# GET TOP PAGES PER HOUR
top_pages <- hourly_df
top_pages$GET_URL <- gsub("^\\S+\\s+|\\s+\\S+$", "", top_pages$GET_URL)
top_pages <- top_pages %>% group_by(LINK = GET_URL, REFERRAL, DATE, HOUR) %>% summarise(HITS = n())
# top_pages$URL <- paste(top_pages$REFERRAL,substring(top_pages$LINK,first = regexpr(pattern = "/",text = top_pages$LINK)+1))
top_pages$URL <- top_pages$LINK
top_pages <- top_pages[, !(names(top_pages) %in% c("LINK", "REFERRAL"))]

# REMOVE RECORDS WITH LESS THAN 50 HITS
# top_pages <- top_pages[(top_pages$HITS > 50),]

# ORDER THE RECORDS BY DATE AND HOUR
top_pages <- top_pages[order(top_pages$DATE, top_pages$HOUR, decreasing = FALSE),]

# REORDER THE COLUMNS
top_pages$CATEGORY <- "VIEWS_PER_PAGE"
top_pages <- top_pages[,c(1,2,5,4,3)]


########################################
##### UNIQUE IP DATA ###################
########################################
# GET UNIQUE IP ADDRESS COUNT PER PLATFORM
ip_hourly_count <- hourly_df
# SEPARATE THEM BY PLATFORM
ip_hourly_count$VALUE <- ifelse(grepl("bot|crawl",x = ip_hourly_count$AGENT,ignore.case = TRUE), "Bot/Other",
                         ifelse(grepl("Android|Mobile|iP(hone,ad,od),",x = ip_hourly_count$AGENT,ignore.case = TRUE),"Mobile",
                         no =  ifelse(grepl("Windows Nt|Mac|Linux|CrOS|Darwin",x = ip_hourly_count$AGENT, ignore.case = TRUE),"Desktop","Bot/Other")))

# REMOVE NON-UNIQUE IPS IN THE SAME DAY/HOUR/PLATFORM AND SUMMARISE
unique_ips <- ip_hourly_count %>% select(IP_ADDRESS, DATE, HOUR, VALUE) %>% distinct(IP_ADDRESS, DATE, HOUR, VALUE) %>% group_by(DATE, HOUR, VALUE) %>% summarise(HITS = n())
# REORDER BY DATE AND HOUR
unique_ips_hourly_count <- unique_ips[order(unique_ips$DATE, unique_ips$HOUR, decreasing = FALSE),]
# ADD IN CATEGORY COLUMN
unique_ips_hourly_count$CATEGORY <- "UNIQUE_IP_ADDRESSES"
# REORDER COLUMNS TO BE CONSISTENT WITH OTHER DATA
unique_ips_hourly_count <- unique_ips_hourly_count[,c(1,2,5,3,4)]


first_hour <- hourly_df[1,]
last_hour <- hourly_df[nrow(hourly_df),]
output_file <- paste0(first_hour$DATE, "-", first_hour$HOUR, "-to-", last_hour$DATE, "-", last_hour$HOUR, ".csv")
write.table(platform_hourly_count, output_file, row.names=FALSE, col.names=TRUE, sep=",")
write.table(condensed_hourly_count, output_file, row.names=FALSE, col.names=FALSE, sep=",", append=TRUE)
write.table(top_pages, output_file, row.names=FALSE, col.names=FALSE, sep=",", append=TRUE)
write.table(unique_ips_hourly_count, output_file, row.names=FALSE, col.names=FALSE, sep=",", append=TRUE)
