daysInMonthForDate <- function(d = Sys.Date()){
  m = substr((as.character(d)), 6, 7)              # month number as string
  y = as.numeric(substr((as.character(d)), 1, 4))  # year number as numeric
  # Quick check for leap year
  leap = 0
  if ((y %% 4 == 0 & y %% 100 != 0) | y %% 400 == 0)
    leap = 1
  # Return the number of days in the month
  return(switch(m, '01' = 31, '02' = 28 + leap, '03' = 31,'04' = 30,'05' = 31,'06' = 30,'07' = 31,'08' = 31,'09' = 30,'10' = 31,'11' = 30,'12' = 31))
}

getFilesStartingWithDate <- function(date) {
  # fetch files starting with date
  file_pattern <- paste0(date, "-\\d{2}-to-", "\\d{4}-\\d{2}-\\d{2}-\\d{2}", ".csv")
  fileListStartingOnDate <- list.files(path=pathToCsv, pattern=file_pattern, full.names=TRUE)
  return(fileListStartingOnDate)
}

getFilesEndingWithDate <- function(date) {
  # fetch files ending with date
  file_pattern <- paste0("\\d{4}-\\d{2}-\\d{2}-\\d{2}-to-", date, "-\\d{2}", ".csv")
  fileListEndingOnDate <- list.files(path=pathToCsv,pattern=file_pattern, full.names=TRUE)
  return(fileListEndingOnDate)
}
