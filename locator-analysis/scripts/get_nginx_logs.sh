#!/bin/bash
##Created by Pavan Kumar, this script fetch the access log file from the locator proxy server by today's date and unzip it. 
#set -x
ID="bkuser"
LOCATOR_PRX_HOSTNAME="bhprd1.eagletechva.com"
LOCATOR_NGX_LOG_REMOTE_PATH="/var/log/nginx/"
DATE=$(date +%F-%T)
LOG_FILE_DATE=$(date +%Y%m%d)

LOCAL_PATH="/bk3/locator_R_analysis/docker_volume/nginx_log_files"
EMAIL_ID=pavan.kumar@eagletechva.com

function get_locator_nginx_log_files {

if [[ -f ${LOCAL_PATH}/access.log-${LOG_FILE_DATE}.gz ]]
then

rm -f ${LOCAL_PATH}/access.log-${LOG_FILE_DATE}.gz

fi


scp ${ID}@${LOCATOR_PRX_HOSTNAME}:${LOCATOR_NGX_LOG_REMOTE_PATH}/access.log-${LOG_FILE_DATE}.gz ${LOCAL_PATH}

if [[ $? -ne 0 ]]
then

	echo "BHPRD1 Nginx Log Access SCP is Failed: Please check" | mailx -s "FAILED: bhprd1 Nginx log scp to backup server is Failed" pavan.kumar@eagletechva.com

else 
	 echo "BHPRD1 Nginx Log Access SCP is Successed" | mailx -s "Success: bhprd1 Nginx log scp to backup server successed" pavan.kumar@eagletechva.com

fi

gunzip -f ${LOCAL_PATH}/access.log-${LOG_FILE_DATE}.gz

if [[ $? -ne 0 ]]
then

        echo "BHPRD1 Nginx Log Access gunzip is Failed: Please check" | mailx -s "FAILED: bhprd1 Nginx Log Access gunzip is Failed" pavan.kumar@eagletechva.com

else
         echo "BHPRD1 Nginx Log Access gunzip is Successed" | mailx -s "Success: bhprd1 Nginx Log Access gunzip is Success" pavan.kumar@eagletechva.com

fi


}

function checking_if_old_log_file_exist {

OLD_LOG_FILE_DATE=$(date --date="yesterday" +%Y%m%d)
echo "Previous day date $OLD_LOG_FILE_DATE"
if [[ -f ${LOCAL_PATH}/access.log-${OLD_LOG_FILE_DATE} ]]
then

	rm -f ${LOCAL_PATH}/access.log-${OLD_LOG_FILE_DATE}

	if [[ $? -eq 0 ]]
	then

        echo "Previous Date BHPRD1 Nginx Log file has been deleted" | mailx -s "Previous Date BHPRD1 Nginx Log file has been deleted" pavan.kumar@eagletechva.com

	else
         echo "Failed: Can't delete the previous date Nginx log file" | mailx -s "Failed: Can't delete the previous date nginx log file" pavan.kumar@eagletechva.com

	fi


else 

echo "Previous day file does not exist"

fi

}

get_locator_nginx_log_files

checking_if_old_log_file_exist
