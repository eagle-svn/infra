#!/bin/sh
DB_NAME=etlocator
function create_db () {
whoami
echo "Status of Postgres:  "
/usr/pgsql-9.6/bin/pg_ctl status  -D /var/lib/pgsql/9.6/data 

echo " Running Postgres scripts "

psql -f /var/lib/pgsql/dbdumps/${DB_NAME}_globals_*.sql 
psql -f /var/lib/pgsql/dbdumps/${DB_NAME}_createdb_*.sql 
psql -f /var/lib/pgsql/dbdumps/${DB_NAME}_extensions_*.sql 
pg_restore -d ${DB_NAME} /var/lib/pgsql/dbdumps/${DB_NAME}_FULL_*.dump 
}
create_db
ps -aef |grep postgres
/usr/pgsql-9.6/bin/pg_ctl stop -D /var/lib/pgsql/9.6/data 
