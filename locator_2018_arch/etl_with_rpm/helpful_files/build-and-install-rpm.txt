
# Build SRPM
rpmbuild -v -ba ./rpmbuild/SPECS/locator_db.spec

# Convert SRPM to RPM
rpmbuild --rebuild ./rpmbuild/SRPMS/locator_db-${TODAY}-0.el7.centos.src.rpm

# Build RPM
rpmbuild -v -bb ./rpmbuild/SPECS/locator_db.spec

# Install RPM
rpm -ivh ./rpmbuild/RPMS/x86_64/locator_db-${TODAY}-0.el7.centos.x86_64.rpm