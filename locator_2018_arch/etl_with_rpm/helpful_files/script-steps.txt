1) ETL runs on build server

2) rpmbuild is run on build server
		-> performs validation of data in database on build server
		-> validation passes
		-> creates dumpfile of database
		-> creates SRPM and RPM file on build server

3) Upload SRPM and RPM to Nexus repository from build server

4) Install RPM from Nexus repository to Docker Image
		-> Runs pg_restore of database
		-> performs validation of data in database on docker image
		-> validation passes
		-> rpm successfully installed