####################
### Requirements ###
####################

BUILD SERVER:
	-> postgres installed and running
	-> rpmbuild installed
	-> svn repo containing rpm scripts downloaded
	-> ETL has been run on the server (IBHS database needs to be present for this to work)


TARGET SERVER (docker container/vm/etc..):
	-> postgres installed and running
	-> eagle.repo file copied into /etc/yum.repos.d/eagle.repo




####################
###     Steps    ###
####################

ON BUILD SERVER:
1) Run ETL
	-> ETL will populate locator database


2) Run rpmbuild
	->  rpmbuild will run validation checks to make sure that the data in the database matches the data in the fusion tables
	->  Once validation finishes, rpmbuild will run pg_dump and pg_dumpall to generate dump files
	->  It will package up the dump files and finish


3) Upload newly created rpm to Nexus repo




ON TARGET SERVER:
1) Run yum install
	-> yum install --enablerepo="eagle_repo" locator_db


2) Check that the validation passed
	-> Look at the output from the yum install