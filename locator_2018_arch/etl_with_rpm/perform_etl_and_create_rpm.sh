## Peform etl on the build server
/local/apps/bhsis/locator/trunk/misc/cron-locator/execute_all_etl.sh

## Create rpmbuild directories
mkdir -p ./rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

# Copy locator specfile into SPECS directory
cp locator_db.spec ./rpmbuild/SPECS/locator_db.spec

#### Happens in rpm file now ####
## Create dump files from post-ETL database and copy them to SOURCES directory
#TODAY=`date +%Y%m%d`
#pg_dump -U postgres -d etlocator -Fc | gzip > ./rpmbuild/SOURCES/etlocator_FULL_${TODAY}.dump.gz
#pg_dumpall -U postgres --globals-only --file=./rpmbuild/SOURCES/etlocator_globals_${TODAY}.sql

## Create macros file and define required vars
cat <<EOF >~/.rpmmacros
%_topdir   `pwd`/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

## Perform rpmbuild to generate .rpm file
rpmbuild -v -ba ./rpmbuild/SPECS/locator_db.spec

## Upload RPM to Nexus repo
curl -v --user 'admin:admin123' --upload-file ./rpmbuild/SRPMS/locator_db-${VERSION}-0.el7.centos.src.rpm http://etdev5dp01.eagletechva.com:8081/repository/eagle-yum-repo/locator_db-${VERSION}-0.el7.centos.src.rpm
