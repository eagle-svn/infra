TRUNCATE dea.aims_user,
  dea.batch_detail_extract,
  dea.batch_info,
  dea.month_wise_report_mh,
  dea.prodstat_batch_details,
  dea.prodstat_batch_details_mh,
  dea.prodstat_batch_info,
  dea.prodstat_current_years,
  dea.report_config,
  dea.report_job,
  dea.report_spec,
  dea.report_spec_template_parameter_file
;


ALTER SEQUENCE dea.aims_user_seq RESTART;
ALTER SEQUENCE dea.batch_detail_extract_id_seq RESTART;
ALTER SEQUENCE dea.batch_info_id_seq RESTART;
ALTER SEQUENCE dea.month_wise_report_mh_seq RESTART;
ALTER SEQUENCE dea.prodstat_seq RESTART;
ALTER SEQUENCE dea.report_config_id_seq RESTART;
ALTER SEQUENCE dea.report_job_report_job_id_seq RESTART;
ALTER SEQUENCE dea.report_spec_report_spec_id_seq RESTART;
ALTER SEQUENCE dea.report_spec_template_parameter_file_id_seq RESTART;

