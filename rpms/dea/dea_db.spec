%define _db_date %(date +"%Y%m%d")


Name:           dea_db
Version:        1.10_r3
Release:        0%{?dist}
Summary:        Installs dea db
License:        No License
URL:            https://www.eagletechva.com
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
%description
%{summary}

BuildRequires:  psql
BuildRequires:  curl
BuildRequires:  bash

Requires:       psql
Requires:       bash
Requires:       curl
Requires:       gunzip

#########################################
###########   PREP SECTION   ############
#########################################
%prep
cd %{_topdir}/
rm -rf %{_builddir}/%{name}-%{version}
mkdir %{_builddir}/%{name}-%{version}

#########################################
##########   BUILD SECTION   ############
#########################################
%build

#########################################
#########  INSTALL SECTION   ############
#########################################
%install
## Create dump files from database and copy to BUILDROOT
mkdir -p $RPM_BUILD_ROOT/tmp
pg_dump -U postgres -d etdss -n dss -n teds -n dea -n dsscld -n cld -n common | gzip > $RPM_BUILD_ROOT/tmp/etdea_FULL.dump.gz
pg_dumpall -U postgres --globals-only --file=$RPM_BUILD_ROOT/tmp/etdea_globals.sql


#########################################
##########   POST SECTION   #############
#########################################
%post
# Extract the dump file
gunzip -c $RPM_BUILDROOT/tmp/etdea_FULL.dump.gz > $RPM_BUILD_ROOT/tmp/etdea_FULL.dump

# Install the database
psql -U postgres -d postgres -f $RPM_BUILD_ROOT/tmp/etdea_globals.sql
psql -U postgres -d postgres -c "DROP DATABASE IF EXISTS etdss"
psql -U postgres -d postgres -c "CREATE DATABASE etdss"
psql -U postgres -d etdss -c "CREATE EXTENSION postgis"

# Restore it
psql -U postgres -d etdss -f $RPM_BUILD_ROOT/tmp/etdea_FULL.dump


%clean
echo "cleaning - nothing to do"


%files
%doc
/tmp/etdea_FULL.dump.gz
/tmp/etdea_globals.sql

%changelog
