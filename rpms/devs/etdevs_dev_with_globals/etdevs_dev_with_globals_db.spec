%define _db_date %(date +"%Y%m%d")


Name:           etdevs_dev_with_globals_db
Version:        1.0.0
Release:        0%{?dist}
Summary:        Installs devs db
License:        No License
URL:            https://www.eagletechva.com
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
%description
%{summary}

BuildRequires:  psql
BuildRequires:  curl
BuildRequires:  bash

Requires:       psql
Requires:       bash
Requires:       curl
Requires:       gunzip

#########################################
###########   PREP SECTION   ############
#########################################
%prep
cd %{_topdir}/
rm -rf %{_builddir}/%{name}-%{version}
mkdir %{_builddir}/%{name}-%{version}

#########################################
##########   BUILD SECTION   ############
#########################################
%build

#########################################
#########  INSTALL SECTION   ############
#########################################
%install
## Create dump files from database and copy to BUILDROOT
mkdir -p $RPM_BUILD_ROOT/tmp
pg_dump -U postgres -d etdevs -Fc -n dlt | gzip > $RPM_BUILD_ROOT/tmp/etdevs_FULL.dump.gz
pg_dumpall -U postgres --globals-only --file=$RPM_BUILD_ROOT/tmp/etdevs_globals.sql


#########################################
##########   POST SECTION   #############
#########################################
%post
# Extract the dump file
gunzip -c $RPM_BUILDROOT/tmp/etdevs_FULL.dump.gz > $RPM_BUILD_ROOT/tmp/etdevs_FULL.dump

# Install the database
psql -U postgres -d postgres -f $RPM_BUILD_ROOT/tmp/etdevs_globals.sql
psql -U postgres -d postgres -c "DROP DATABASE IF EXISTS etdevs"
psql -U postgres -d postgres -c "CREATE DATABASE etdevs"
psql -U postgres -d etdevs -c "CREATE EXTENSION postgis"

# Restore it
pg_restore -U postgres -d etdevs $RPM_BUILD_ROOT/tmp/etdevs_FULL.dump


%clean
echo "cleaning - nothing to do"


%files
%doc
/tmp/etdevs_FULL.dump.gz
/tmp/etdevs_globals.sql

%changelog
