%define _db_date %(date +"%Y%m%d")


Name:           dss_db
Version:        4.1_r3
Release:        0%{?dist}
Summary:        Installs dss db
License:        No License
URL:            https://www.eagletechva.com
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
%description
%{summary}

BuildRequires:  psql
BuildRequires:  curl
BuildRequires:  bash

Requires:       psql
Requires:       bash
Requires:       curl
Requires:       gunzip

#########################################
###########   PREP SECTION   ############
#########################################
%prep
cd %{_topdir}/
rm -rf %{_builddir}/%{name}-%{version}
mkdir %{_builddir}/%{name}-%{version}

#########################################
##########   BUILD SECTION   ############
#########################################
%build

#########################################
#########  INSTALL SECTION   ############
#########################################
%install
## Create dump files from database and copy to BUILDROOT
mkdir -p $RPM_BUILD_ROOT/tmp
pg_dump -U postgres -d etdss | gzip > $RPM_BUILD_ROOT/tmp/etdss_FULL.dump.gz
pg_dumpall -U postgres --globals-only --file=$RPM_BUILD_ROOT/tmp/etdss_globals.sql


#########################################
##########   POST SECTION   #############
#########################################
%post
# Extract the dump file
gunzip -c $RPM_BUILDROOT/tmp/etdss_FULL.dump.gz > $RPM_BUILD_ROOT/tmp/etdss_FULL.dump

# Install the database
psql -U postgres -d postgres -f $RPM_BUILD_ROOT/tmp/etdss_globals.sql
psql -U postgres -d postgres -c "DROP DATABASE IF EXISTS etdss"
psql -U postgres -d postgres -c "CREATE DATABASE etdss"

# Restore it
psql -U postgres -d etdss -f $RPM_BUILD_ROOT/tmp/etdss_FULL.dump


%clean
echo "cleaning - nothing to do"


%files
%doc
/tmp/etdss_FULL.dump.gz
/tmp/etdss_globals.sql

%changelog
