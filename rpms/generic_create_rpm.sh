read -p "Specify database to create rpm for (ibhs, devs, dss, dea, or locator: " DATABASE
## Create rpmbuild directories
mkdir -p ./${DATABASE}/rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

## Create macros file and define required vars
cat <<EOF >~/.rpmmacros
%_topdir   `pwd`/${DATABASE}/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

## Get current rpm version
CURRENT_VERSION=`grep "Version:*" ${DATABASE}/${DATABASE}_db.spec`
echo Current RPM ${CURRENT_VERSION}

## Prompt user for new rpm version and replace in file
read -p "Enter new RPM version: " VERSION
sed -i.bak "s/^Version:.*/Version:        ${VERSION}/" ${DATABASE}/${DATABASE}_db.spec && rm ${DATABASE}/${DATABASE}_db.spec.bak

# Copy specfile into SPECS directory
cp ${DATABASE}/${DATABASE}_db.spec ./${DATABASE}/rpmbuild/SPECS/${DATABASE}_db.spec

## Perform rpmbuild to generate .rpm file
rpmbuild -v -bb ./${DATABASE}/rpmbuild/SPECS/${DATABASE}_db.spec

## Upload RPM to Nexus repo
echo "Enter your svn information to upload the rpm to the nexus repo"
read -p "svn username: " USERNAME
read -sp "svn password: " PASSWORD

echo "Updating svn repo spec file to have newest version"
echo ""
svn commit ${DATABASE}/${DATABASE}_db.spec -m "BIT - 3850 - INFRA - Create rpms for ibhs,dss,devs,locator,dsscld

Updated rpm version for ${DATABASE} database"
curl -v --user "${USERNAME}:${PASSWORD}" --upload-file ./${DATABASE}/rpmbuild/RPMS/x86_64/${DATABASE}_db-${VERSION}-0.el7.centos.x86_64.rpm http://etinf21.eagletechva.com:8081/repository/eagle-yum-repo/${DATABASE}_db-${VERSION}-0.el7.centos.x86_64.rpm
