Name:           etlocator_locator_db
Version:        1.0.2
Release:        0%{?dist}
Summary:        Validates and installs locator db after ETL
License:        No License
URL:            https://www.eagletechva.com
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
%description
%{summary}

BuildRequires:  psql
BuildRequires:  curl
BuildRequires:  bash

Requires:       psql
Requires:       bash
Requires:       curl
Requires:       gunzip

#########################################
###########   PREP SECTION   ############
#########################################
%prep
cd %{_topdir}/
rm -rf %{_builddir}/%{name}-%{version}
mkdir %{_builddir}/%{name}-%{version}

#########################################
##########   BUILD SECTION   ############
#########################################
%build

#########################################
#########  INSTALL SECTION   ############
#########################################
%install
## Create dump files from post-ETL database and copy to BUILDROOT
mkdir -p $RPM_BUILD_ROOT/tmp
pg_dump -U postgres -d etlocator | gzip > $RPM_BUILD_ROOT/tmp/etlocator_FULL.dump.gz
pg_dumpall -U postgres --globals-only --file=$RPM_BUILD_ROOT/tmp/etlocator_globals.sql


#########################################
##########   POST SECTION   #############
#########################################
%post
# Extract the dump file
gunzip -c $RPM_BUILDROOT/tmp/etlocator_FULL.dump.gz > $RPM_BUILD_ROOT/tmp/etlocator_FULL.dump

# Install the database
psql -U postgres -d postgres -f $RPM_BUILD_ROOT/tmp/etlocator_globals.sql
psql -U postgres -d postgres -c "DROP DATABASE IF EXISTS etlocator"
psql -U postgres -d postgres -c "CREATE DATABASE etlocator"

# Restore it
psql -U postgres -d etlocator -f $RPM_BUILD_ROOT/tmp/etlocator_FULL.dump


%clean
echo "cleaning - nothing to do"


%files
%doc
/tmp/etlocator_FULL.dump.gz
/tmp/etlocator_globals.sql

%changelog
