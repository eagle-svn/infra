## Peform etl on the build server
/local/apps/bhsis/locator/trunk/misc/cron-locator/execute_all_etl.sh

## Create rpmbuild directories
mkdir -p ./rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

# Copy locator specfile into SPECS directory
cp locator_db.spec ./rpmbuild/SPECS/locator_db.spec

## Create macros file and define required vars
cat <<EOF >~/.rpmmacros
%_topdir   `pwd`/rpmbuild
%_tmppath  %{_topdir}/tmp
EOF

## Perform rpmbuild to generate .rpm file
rpmbuild -v -bb ./rpmbuild/SPECS/locator_db.spec

## Upload RPM to Nexus repo
## DONT FORGET TO BUMP THE VERSION NUMBER IN THE spec file.
read 'Enter your svn information to upload the rpm to the nexus repo'
read -p 'svn username: ' USERNAME
read -sp 'svn password: ' PASSWORD
read -p 'Enter version of rpm: ' VERSION
curl -v --user 'admin:admin123' --upload-file ./rpmbuild/RPMS/x86_64/locator_db-${VERSION}-0.el7.centos.x86_64.rpm http://etinf21.eagletechva.com:8081/repository/eagle-yum-repo/locator_db-${VERSION}-0.el7.centos.x86_64.rpm
