#!/bin/bash
##Created by Pavan Kumar
###Purpose of this script is to start the Rserver by using the systemmd


DATE=$(date  +"%m-%d-%Y-%H-%M-%S")

RSERVE_LOG_PATH=/local/apps/bhsis/RServe_logs/Rserve.log


function usage () {

if [ "$#" -eq 0 ]
then

echo "Usage: pass the start or stop argument\n"

exit 1

elif [ "$#" -ge 2 ]
then

echo "Please pass only the stat and stop argument"

exit 1

else

:

fi

}

if [ "$1" == "start" ]
then

mv ${RSERVE_LOG_PATH} ${RSERVE_LOG_PATH}_${DATE}

R CMD Rserve --no-save > ${RSERVE_LOG_PATH}  2>&1

        if [ "$?" -ne 0 ]
        then
                echo "FAILED: RServer has not been started, please check"
                exit 1

        fi

elif [ "$1" == "stop" ]

then

killall Rserve

        if [ "$?" -ne 0 ]
        then
                echo "FAILED: RServer has not been stopped, please check or kill manually"
                exit 1

        fi

else

        usage

fi


