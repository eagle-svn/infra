#!/bin/bash
##Created By Pavan Kumar
##This script copies the nginx access log files of locator to backup server 

ID="bkuser"
LOCATOR_PRX_HOSTNAME="bhprd1.eagletechva.com"
LOCATOR_NGX_LOG_REMOTE_PATH="/var/log/nginx/access.*"
DATE=$(date +%F-%T)
LOG_FILE=/home/bkuser/scripts/logs/bhprd1_ngx_access_rsync_${DATE}.log
LOCATOR_NGX_LOG_LOCAL_PATH="/bk1/bhprd1_nginx_logs"
EMAIL_ID=pavan.kumar@eagletechva.com

function get_locator_nginx_log_files {

rsync --ignore-existing  -r -a -v -e ssh ${ID}@${LOCATOR_PRX_HOSTNAME}:${LOCATOR_NGX_LOG_REMOTE_PATH} ${LOCATOR_NGX_LOG_LOCAL_PATH} --log-file=${LOG_FILE}



}

function checking_the_error_on_log_file {

FOUND_ERROR=$(grep -Ei "error| failed | fail" ${LOG_FILE})

if [[ ${FOUND_ERROR} != "" ]]
then

echo "BHPRD1 Nginx Log Access rsync is Failed: Please check" | mailx -s "FAILED: bhprd1 Nginx log copy rsync to backup server is Failed" pavan.kumar@eagletechva.com 


else 

echo "BHPRD1 Nginx Log Access rsync is completed" | mailx -s "SUCCESS: bhprd1 Nginx log copy rsync to backup server is completed" pavan.kumar@eagletechva.com
fi

}

get_locator_nginx_log_files

checking_the_error_on_log_file
