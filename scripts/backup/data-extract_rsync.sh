#!/bin/bash
##Created By Pavan Kumar
##Purpose of this script is to sync the files from data-extract application server to backup server.

REMOTE_PATH=/data-extract/

LOCAL_PATH=/bk3/data-extract/

DATE=$(date +%F-%T)

LOG_FILE=/tmp/data-extract_rsync_${DATE}.log

REMOTE_HOST=bhprd2app01.eagletechva.com

ID=bkuser


rsync --delete -avzhe  ssh ${ID}@${REMOTE_HOST}:${REMOTE_PATH} ${LOCAL_PATH} --log-file=${LOG_FILE}


ERROR_MESSAGE=$(grep -iE "error|fail|failed" ${LOG_FILE})

if [[ ${ERROR_MESSAGE} == "" ]]
then
	echo "Rsych has been complete" | mailx -s "test" pavan.kumar@eagletechva.com

else

	echo -e "Failed\n\n ${ERROR_MESSAGE}" |  mailx -s "test" pavan.kumar@eagletechva.com

fi

echo "LOG FILE $LOG_FILE"
