#!/bin/sh
##Created by Pavan Kumar and Mubarak
## Purpose of this script is to backup the database dump to backup server.

DB_BK_HOST_NAME=bhprd1app02.eagletechva.com
DB_BK_HOST_ID=bkuser
DB_BACKUP_PATH="/pgbackup/logical_backups/"
BK_SERVER_PATH="/bk1/bk_bhprd1app02/"
DATE=$(date +%Y-%m-%d_%H-%M-%S)
EMAIL_ADDR="pavan.kumar@eagletechva.com, bhsis_infra@eagletechva.com, shailaja.arkacharya@eagletechva.com"
#EMAIL_ADDR="pavan.kumar@eagletechva.com"
HOST_NAME=$(hostname | awk -F. '{print $1}')


LOG_FILE=/tmp/database_dump_backup_of_${DB_BK_HOST_NAME}_${DATE}.log

if [[ -f ${LOG_FILE} ]]
then

rm -f ${LOG_FILE}

else

:

fi

rsync  -avzh --progress --backup --suffix=.bak -e ssh ${DB_BK_HOST_ID}@${DB_BK_HOST_NAME}:${DB_BACKUP_PATH} ${BK_SERVER_PATH}  --log-file=${LOG_FILE}


RSYNC_FAILED=$(grep "failed" ${LOG_FILE}) 



RSYNC_ERROR=$(grep "error" ${LOG_FILE})


if [[ "${RSYNC_FAILED}" == "" ]] && [[ "${RSYNC_ERROR}" == "" ]]
then

STATUS=SUCCESSFUL

else

STATUS=FAILED

fi


MESSAGE="Database Backup on ${HOST_NAME} from  ${DB_BK_HOST_NAME} : ${STATUS}"

echo "$MESSAGE" | mail -s "${MESSAGE}" ${EMAIL_ADDR} < ${LOG_FILE}


