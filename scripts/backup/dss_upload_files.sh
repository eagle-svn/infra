#!/bin/bash
##Created By Pavan Kumar
##This script copies the nginx access log files of locator to backup server 

ID="bkuser"
APP_HOSTNAME="bhprd2app01.eagletechva.com"
DSS_UPLOAD_REMOTE_PATH="/local/apps/bhsis/dss_upload/*"
DATE=$(date +%F-%T)
LOG_FILE=/home/bkuser/scripts/logs/dss_upload_rsync_${DATE}.log
DSS_UPLOAD_LOCAL_PATH="/bk1/dss_upload_files"
EMAIL_ID=pavan.kumar@eagletechva.com

function get_dss_upload_files {

rsync --ignore-existing  -r -a -v -e ssh ${ID}@${APP_HOSTNAME}:${DSS_UPLOAD_REMOTE_PATH} ${DSS_UPLOAD_LOCAL_PATH} --log-file=${LOG_FILE}



}

function checking_the_error_on_log_file {

FOUND_ERROR=$(grep -Ei "error| failed | fail" ${LOG_FILE})

if [[ ${FOUND_ERROR} != "" ]]
then

echo "DSS Upload File rsync is Failed: Please check" | mailx -s "FAILED: DSS Upload File rsync to backup server is Failed" ${EMAIL_ID} 


else 

echo "DSS Upload rsync is completed" | mailx -s "SUCCESS: DSS Upload rsync to backup server is completed" ${EMAIL_ID}
fi

}

get_dss_upload_files
checking_the_error_on_log_file

