#!/bin/bash
# script to backup individual database
EMAIL_LIST="shailaja.arkacharya@eagletechva.com,cody.cummings@eagletechva.com "
# Log location

# Backup Retention Window
RETENTION_DAYS=7

# Backup location.
LOG_FILE_DIR=/pgbackup/logs
RETAINED_DIR=/pgbackup/retained_backups
LOGICAL_DIR=/pgbackup/logical_backups

# parameters set
DB_NAME=$1
LOG_FILE=/pgbackup/logs/logical_backup.log
YESTERDAY=`date -d "yesterday" +'%Y%m%d'`
TODAY=`date +%Y%m%d`
# source the bash file
source /pgsql/.bash_profile

set -u -o pipefail

# Ensure backup path exists.
mkdir -p -m 750 "${LOG_FILE_DIR}"
mkdir -p -m 750 "${LOGICAL_DIR}"
mkdir -p -m 750 "${RETAINED_DIR}"

# Initialize log with redirect of all stdout and stderr.
exec > "${LOG_FILE}" 2>&1
echo `date +%Y-%m-%d\ %r:\ `"PostgreSQL Backup: "`hostname`
echo ""


# Archive prior day's schema extensions backup.
echo `date +%Y-%m-%d\ %r:\ `"archive prior extensions backup: starting..."
if  [ -f ${LOGICAL_DIR}/${DB_NAME}_extensions_${YESTERDAY}.sql ];
then
    mv ${LOGICAL_DIR}/${DB_NAME}_extensions_${YESTERDAY}.sql ${RETAINED_DIR}/${DB_NAME}_extensions_${YESTERDAY}.sql
fi
    echo `date +%Y-%m-%d\ %r:\ `"archive prior extensions backup: complete."
        echo ""

echo `date +%Y-%m-%d\ %r:\ `"archive prior globals backup: starting..."
if  [ -f ${LOGICAL_DIR}/${DB_NAME}_globals_${YESTERDAY}.sql ];
then
    mv ${LOGICAL_DIR}/${DB_NAME}_globals_${YESTERDAY}.sql ${RETAINED_DIR}/${DB_NAME}_globals_${YESTERDAY}.sql
fi
        echo `date +%Y-%m-%d\ %r:\ `"archive prior globals backup: complete."
        echo ""

# Archive prior day's FULL backup.
echo `date +%Y-%m-%d\ %r:\ `"archive prior logical full backup: starting..."
if  [ -f ${LOGICAL_DIR}/${DB_NAME}_FULL_${YESTERDAY}.dump ];
then
    gzip ${LOGICAL_DIR}/${DB_NAME}_FULL_${YESTERDAY}.dump
    mv ${LOGICAL_DIR}/${DB_NAME}_FULL_${YESTERDAY}.dump.gz ${RETAINED_DIR}/${DB_NAME}_FULL_${YESTERDAY}.dump.gz
fi
        echo `date +%Y-%m-%d\ %r:\ `"archive prior FULL logical backup: complete."
        echo ""

# Archive prior day's create DB backup.
echo `date +%Y-%m-%d\ %r:\ `"archive prior create db backup: starting..."
if  [ -f ${LOGICAL_DIR}/${DB_NAME}_createdb_${YESTERDAY}.sql ];
then
    mv ${LOGICAL_DIR}/${DB_NAME}_createdb_${YESTERDAY}.sql ${RETAINED_DIR}/${DB_NAME}_createdb_${YESTERDAY}.sql
fi
        echo `date +%Y-%m-%d\ %r:\ `"archive prior create db  backup: complete."
        echo ""

# Generate new base backup.
echo `date +%Y-%m-%d\ %r:\ `"logical backup: starting..."

pg_dumpall -U postgres --globals-only --file=${LOGICAL_DIR}/${DB_NAME}_globals_${TODAY}.sql

RETURN_CODE=$?
if [ $RETURN_CODE -ne 0 ]; then
    echo "ERROR: Failed to export all database users and roles."
    exit 1
fi

sed -i '/^DROP  ROLE postgres;/ d' ${LOGICAL_DIR}/${DB_NAME}_globals_${TODAY}.sql
if [ $RETURN_CODE -ne 0 ]; then
    echo "ERROR: Failed to remove postgres drop statement from role file."
    exit 1
else
    echo `date +%Y-%m-%d\ %r:\ `"role script: file complete."
fi

sed -i '/^CREATE ROLE postgres;/ d' ${LOGICAL_DIR}/${DB_NAME}_globals_${TODAY}.sql

RETURN_CODE=$?
if [ $RETURN_CODE -ne 0 ]; then
    echo "ERROR: Failed to remove postgres create statement from role file."
    exit 1
else
    echo `date +%Y-%m-%d\ %r:\ `"role script: file complete."
fi
pg_dump -U postgres  --file=${LOGICAL_DIR}/${DB_NAME}_extensions_${TODAY}.sql
RETURN_CODE=$?
if [ $RETURN_CODE -ne 0 ]; then
    echo "ERROR: Failed to create extensions."
    exit 1
else
    echo `date +%Y-%m-%d\ %r:\ `"Extensions script: file complete."
fi
sed -i '/^DROP SCHEMA public;/ d' ${LOGICAL_DIR}/${DB_NAME}_extensions_${TODAY}.sql

RETURN_CODE=$?
if [ $RETURN_CODE -ne 0 ]; then
    echo "ERROR: Failed to remove drop schema public statement from extensions file."
    exit 1
else
    echo `date +%Y-%m-%d\ %r:\ `"extension script: file complete."
fi


##################################################################
# -----------------------------------------------------------------------------
# 2. Script to recreate the database.
# -----------------------------------------------------------------------------
echo `date +%Y-%m-%d\ %r:\ `"database script: generating file..."
# database
echo "
create database ${DB_NAME}
    with owner = postgres
    encoding = 'UTF8'
    tablespace = pg_default
    lc_collate = 'en_US.UTF-8'
    lc_ctype = 'en_US.UTF-8'
    connection limit = -1;
" > ${LOGICAL_DIR}/${DB_NAME}_createdb_${TODAY}.sql
# search_path
psql --no-align --tuples-only -d ${DB_NAME} \
--command="select   'alter database ' || current_database()
                 || ' set search_path = ' || 'public,'
                 || string_agg(nspname,',' order by nspname)
                 || ';' as search_path_list
           from pg_namespace
           where nspname not in
                               ('pg_toast'
                               ,'pg_temp_1'
                               ,'pg_toast_temp_1'
                               ,'public'
                               )
             and nspname not like '%_history'
             and nspname not like '%_source'
             and nspname not like '%_stage';
" >> ${LOGICAL_DIR}/${DB_NAME}_createdb_${TODAY}.sql
# user database access control list
psql --no-align --tuples-only -d ${DB_NAME} \
--command="select   'grant ' || string_agg(acl_item,',')
                 || ' on database ${DB_NAME} to '
                 || coalesce(
                             (select rolname
                              from pg_roles where oid = grantee
                             )
                             ,'public'
                            ) || ';' as grant_sql
           from (select lower((aclexplode(datacl)).privilege_type) as acl_item
                       ,((aclexplode(datacl)).grantee) as grantee
                 from   pg_database
                 where  datname = '${DB_NAME}'
                ) as acl_list
           group by grantee
           order by coalesce((select rolname
                              from pg_roles where oid = grantee
                             )
                            ,'public'
                            );
" >> ${LOGICAL_DIR}/${DB_NAME}_createdb_${TODAY}.sql

pg_dump -U postgres  -d ${DB_NAME} \
         --exclude-schema='public' \
         --format=custom \
         --file ${LOGICAL_DIR}/${DB_NAME}_FULL_${TODAY}.dump
RETURN_CODE=$?
if [ $RETURN_CODE -ne 0 ]; then
    echo "ERROR: Failed to create FULL backup of ${DB_NAME}."
    exit 1
else
    echo `date +%Y-%m-%d\ %r:\ `"Full backup file complete."
fi

# Remove backups outside retention window (base backup and WAL backup).
echo `date +%Y-%m-%d\ %r:\ `"removing backups outside retention window: starting..."
RETENTION_DATE=`date +%Y%m%d -d "${RETENTION_DAYS} days ago"`
if  [ -f ${RETAINED_DIR}/${DB_NAME}_FULL_${RETENTION_DATE}.dump.gz ] && [ -f ${RETAINED_DIR}/${DB_NAME}_createdb_${RETENTION_DATE}.sql ];
then
    echo "removing: ${DB_NAME}_FULL_${RETENTION_DATE}.dump.gz"
    rm ${RETAINED_DIR}/${DB_NAME}_FULL_${RETENTION_DATE}.dump.gz
    echo "removing: ${DB_NAME}_createdb_${RETENTION_DATE}.sql"
    rm ${RETAINED_DIR}/${DB_NAME}_createdb_${RETENTION_DATE}.sql
    echo "removing: ${DB_NAME}_extensions_${RETENTION_DATE}.sql"
    rm ${RETAINED_DIR}/${DB_NAME}_extensions_${RETENTION_DATE}.sql
    echo "removing: ${DB_NAME}_globals_${RETENTION_DATE}.sql"
    rm ${RETAINED_DIR}/${DB_NAME}_globals_${RETENTION_DATE}.sql
fi
echo `date +%Y-%m-%d\ %r:\ `"removing backups outside retention window: complete."
echo ""

mail -s "bhprd3db01 Nightly Backup" ${EMAIL_LIST} < ${LOG_FILE}
