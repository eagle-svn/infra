#!/usr/bin/env python

from bs4 import BeautifulSoup
import requests
import urllib3
import re
import getpass
import time
from requests.auth import HTTPBasicAuth


#login_url = 'https://etqa9.eagletechva.com/docsite'
login_url = 'https://etqa9.eagletechva.com/doc/default.htm'
#a_url = 'https://etqa4.eagletechva.com/dasis2/'
#url = 'https://etqa4.eagletechva.com/dasis2/index.htm'
#home_url = 'https://etqa9.eagletechva.com/docsite/callback'
home_url = 'https://etqa9.eagletechva.com/doc/default.htm'
#a_url = 'https://etqa9.eagletechva.com/docsite/docProxy/'
a_url = 'https://etqa9.eagletechva.com/doc/'
urllib3.disable_warnings()
login_data = dict()

username = input('Enter the user name: ')
password = getpass.getpass(prompt='Enter the password: ')

#login_data['username'] = username
#login_data['password'] = password



headers = { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0' }

## Possible of my first class 
#def get_soup_and_session(login_url, home_url, login_data):
#  ''' Return the home page link and session takes three arugment, login_url, home_url, login_data. '''

#  with requests.Session() as req_session:
#    login_page = req_session.get(login_url, headers=headers, verify=False)
#    login_soup = BeautifulSoup(login_page.content, 'html5lib')
#    csrf_token = login_soup.find('input', attrs= { 'name': 'csrfToken' })['value']
#    login_data['csrfToken'] = csrf_token
#    login_post = req_session.post(home_url, headers=headers, verify=False, data=login_data)
#    home_page = BeautifulSoup(login_post.content, 'html5lib')
#    #print(home_page)
#    return home_page, req_session


def get_soup_and_session(login_url, home_url, login_data):
  ''' Return the home page link and session takes three arugment, login_url, home_url, login_data. '''

  with requests.Session() as req_session:
    req_session.auth = (username, password)
    #login_page = req_session.get(login_url, headers=headers, verify=False, auth=HTTPBasicAuth(username, password))
    login_page = req_session.get(login_url, headers=headers, verify=False)
    login_soup = BeautifulSoup(login_page.content, 'html5lib')
    return login_soup, req_session

def get_html_links(b_soup, relative_path=None):
  local_links = set()
  outside_links = []
  no_match_index = re.compile(r'^(?!index\.*)')
  no_match_default = re.compile(r'^(?!default\.*)')
  no_match_mail = re.compile(r'^(?!mail\.*)')
  match_hash_img = re.compile(r'\.htm#|img|\.\.')
  match_img = re.compile(r'img')
  match_http = re.compile(r'http|https')
  match_html = re.compile(r'\.htm|\.html')

  for link in b_soup.findAll('a'):
    if re.search(match_hash_img, str(link)):
      continue
    else: 
      try:
        if re.search(match_http, str(link.get('href'))):
          outside_links.append(link.get('href'))

        else:
          if re.search(no_match_index, link.get('href')) and re.search(no_match_mail, link.get('href')) and re.search(no_match_default, link.get('href')):
            if relative_path:
              local_links.add(a_url + relative_path + '/' + link.get('href'))
            else:
              local_links.add(a_url + link.get('href'))
      except Exception as e:
        pass
  
  local_links = list(local_links)
  return local_links, outside_links 

b_soup, session = get_soup_and_session(login_url, home_url, login_data)
l_links, o_links = get_html_links(b_soup)

def get_sub_links(rest_links):
  other_links = []
  for sub_link in rest_links:
    ex_pattern = re.compile(r'\.pdf$|\.zip$|\.xls$|\.pptx$|\.mp4$|\.wmv$|\.xlsx$', re.IGNORECASE)
    if re.search(ex_pattern, sub_link):
      continue
    else:
      sub_path = sub_link.replace(a_url, '')
      paths = sub_path.split('/')[:-1]
      if len(paths) >= 1:
        if '..' not in paths:
          relative_path = "/".join(paths)
          sub_link = session.get(sub_link, headers=headers, verify=False)
          sub_soup = BeautifulSoup(sub_link.content, features="lxml")
          sl_links, so_links = get_html_links(sub_soup, relative_path=relative_path)
      else:
        sub_link = session.get(sub_link, headers=headers, verify=False)
        sub_soup = BeautifulSoup(sub_link.content, features="lxml")
        sl_links, so_links = get_html_links(sub_soup)
    for s_link in sl_links:
      if s_link not in l_links:
        if re.search('\.htm|\.html', s_link):
          other_links.append(s_link)
          l_links.append(s_link)
  print('Time Sleep 5 seconds', other_links)
  if other_links:
    return other_links
  else:
    return 

my_lists = get_sub_links(l_links)

while True:
  if my_lists:
     my_lists = get_sub_links(my_lists)
     print('I am here')
  else:
    print('I have got the none')
    break

def get_html_status(html_links):
  ex_pattern = re.compile(r'\.pdf$|\.zip$|\.xls$|\.pptx$|\.mp4$|\.wmv$|\.xlsx$', re.IGNORECASE)
  for html_link in html_links:
    _, session = get_soup_and_session(login_url, home_url, login_data)
    reqs = session.head(html_link, headers=headers, verify=False)
    print(html_link, reqs.status_code)
    print('Getting the inner link')
    if re.search(ex_pattern, html_link):
      reqs = session.head(html_link, headers=headers, verify=False)
      print(inner_link, reqs.status_code)
    else:
      b_soup, session = get_soup_and_session(html_link, home_url, login_data)
      sub_path = html_link.replace(a_url, '')
      paths = sub_path.split('/')[:-1]
      if len(paths) >= 1:
        relative_path = "/".join(paths)
        print('My_releative_path', relative_path)
        inner_links, outer_links = get_html_links(b_soup, relative_path=relative_path)
      else:
        sl_links, so_links = get_html_links(b_soup)
        inner_links, outer_links = get_html_links(b_soup)
    
    for inner_link in inner_links:
      if inner_link not in html_links:
        reqs = session.head(html_link, headers=headers, verify=False)
        if reqs.status_code == 200:
          print(inner_link, reqs.status_code)


get_html_status(l_links)
