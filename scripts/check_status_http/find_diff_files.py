#!/usr/bin/env python3
'''
Created by Pavan Kumar
Purpose of the script is to get the doc-stie and dasis_home page files
'''
import re
import argparse
import os

##defining the parser arguments
parser = argparse.ArgumentParser(description='Pass the required arguments')
parser.add_argument('-p', '--project', help='doc_site or dasis_home as the project', required=True)
parser.add_argument('-e', '--email_addresses', help='Enter the email addresses with command separated')
parser.add_argument('-s', '--save_files_dir', help='Enter the directory path where you want to save the files')

args = parser.parse_args()

if args.project:
    if args.project == 'doc_site' or args.project == 'dasis_home':
      project = args.project
    else:
      logging.CRITICAL('Pass the project argument as doc_site or dasis_home only')
      raise Exception('Pass the project argument as doc_site or dasis_home only')

if args.save_files_dir:
    save_files_dir = args.save_files_dir
else:
    save_files_dir = '.'

if args.email_addresses:
    toaddrs = args.email_addresses.replace(" ", "").split(',')

else:
    toaddrs = ['pavan.kumar@eagletechva.com']

fromaddr = "pavan.kumar@eagletechva.com"

re_pattern = re.compile(r'\.csv$|\.gif$|\.jpg$|\.png$', re.IGNORECASE )

def create_diff_set_file(project):
  from_os = ('data/' + project + '_web_files_from_os.txt')
  from_web = ('data/' + project + '_full_links_data.txt')
  file_name = ('data/' + project + '_difference_in_data.txt')
  with open(from_os, 'r') as os_f:
    os_file = os_f.readlines()

  with open(from_web, 'r') as web_f:
    web_file = web_f.readlines()

  os_set = set()
  os_file = sorted(os_file)
  web_file = sorted(web_file)

  for v in os_file:
    os_set.add(v)
  web_set = set()
  for w in web_file:
    web_set.add(w)

  print('Length of OS', len(os_set))
  print('Length of web_vile', len(web_set))
  diff_set = os_set - web_set
  print('Length of diff set' , len(diff_set))

  if os.path.exists(file_name):
    os.remove(file_name)
  
  with open(file_name, 'a') as d_f:
    for ds in sorted(diff_set):
      if re.search(re_pattern, ds) is None:
        d_f.write('{}'.format(ds))


if project == 'doc_site' or project == 'dasis_home':
  create_diff_set_file(project)
else:
  print('You have not specify the project Name')
