#!/usr/bin/env python3
'''
Created by Pavan Kumar
Purpose of the script is to get the doc-stie and dasis_home page files
'''
import re
import argparse
import os
from pexpect import pxssh
import getpass

##defining the parser arguments
parser = argparse.ArgumentParser(description='Pass the required arguments')
parser.add_argument('-p', '--project', help='doc_site or dasis_home as the project', required=True)
parser.add_argument('-e', '--email_addresses', help='Enter the email addresses with command separated')
parser.add_argument('-s', '--save_files_dir', help='Enter the directory path where you want to save the files')

args = parser.parse_args()

if args.project:
    if args.project == 'doc_site' or args.project == 'dasis_home':
      project = args.project
    else:
      logging.CRITICAL('Pass the project argument as doc_site or dasis_home only')
      raise Exception('Pass the project argument as doc_site or dasis_home only')

if args.save_files_dir:
    save_files_dir = args.save_files_dir
else:
    save_files_dir = '.'

if args.email_addresses:
    toaddrs = args.email_addresses.replace(" ", "").split(',')

else:
    toaddrs = ['pavan.kumar@eagletechva.com']

fromaddr = "pavan.kumar@eagletechva.com"

doc_site_host = 'etqa4web01'
dasis_home_host = 'etqa4web02'
doc_site_dir = '/var/www/html/docsite'
dasis_home_dir = '/var/www/html/dasis2'
doc_site_URL = 'https://etqa9.eagletechva.com/doc'
dasis_home_URL = 'https://etqa4.eagletechva.com/dasis2'


username = input('unix username: ')
password = getpass.getpass('unix password: ')

def get_web_app_files(username, password, hostname, dir_path):
  try:
    s = pxssh.pxssh()
    s.login(hostname, username, password)
    command = ('find ' + dir_path + ' -type f')
    s.sendline(command)
    s.prompt()
    web_app_files = s.before.decode()
    s.logout()
  except pxssh.ExceptionPxssh as e:
    print("pxssh failed on login.")
    print(e)

  return web_app_files

if project == 'doc_site':
  web_app_data = get_web_app_files(username, password, doc_site_host, doc_site_dir)
  web_files = web_app_data.split('\r\n')
  file_name = ('data/' + project + '_web_files_from_os.txt') 
  with open(file_name, 'w') as d_w:
    for web_file in web_files[1:]:
      if '.svn' not in web_file:
        web_file = re.sub(doc_site_dir, doc_site_URL, web_file)
        d_w.write('{}\n'.format(web_file))

elif project == 'dasis_home':
  web_app_data = get_web_app_files(username, password, dasis_home_host, dasis_home_dir)
  file_name = ('data/' + project + '_web_files_from_os.txt')
  web_files = web_app_data.split('\r\n')
  with open(file_name, 'w') as d_w:
    for web_file in web_files[1:]:
      if '.svn' not in web_file:
        web_file = re.sub(dasis_home_dir, dasis_home_URL, web_file)
        d_w.write('{}\n'.format(web_file))
else:
  raise Exception('You have not selceted the project')


