#!/bin/sh
##Created by Pavan Kumar


export DATE=$(date +%m%d%y_%H%M%S)
export DB_DUMP_DATE=$(date +%Y%m%d)
export LOG_FILE="/tmp/dump_database_to_other_servers_${DATE}.log"


. /local/apps/bhsis/locator_scripts/dump_database_to_other_servers.properties

function usage() {
printf "Purpse of this script:
1. This script takes the DB dump from set A primary host or from set primary host B,and scp to the other servers of that set. 
2. Drop the etlocator database, and restore the database to servers. 
 
Set A server names are ${SET_A_PRIMARY_HOST}, ${SET_A_OTHER_HOSTS}
and Primary host of set A is: ${SET_A_PRIMARY_HOST}

Set B server names are ${SET_B_PRIMARY_HOST}, ${SET_B_OTHER_HOSTS}
and Primary host of set B is: ${SET_B_PRIMARY_HOST}\n

To use this script please pass the A or B\n"  

}

usage


if [[ $# -eq "" ]]

then

usage

elif [[ $# -gt 1 ]]
then
usage
exit 1

else

:

fi

if [[ $1 == "A" ]] || [[ $1 == "B" ]];then

printf " Your argument value is $1 \n" |tee -a ${LOG_FILE}
export ARG=$1
else
printf "Failed: Please provide the argument A or B only\n" |tee -a ${LOG_FILE}


exit 1

fi

printf "\nEnter your unix ID: "
read USER_ID
export USER_ID;

printf "\nEnter your unix ID password: "

read  -rs ID_PASS

export ID_PASS;


function make_db_dump_and_scp (){

	if [[ $ARG == "A" ]];then
	
	printf "Taking the DB dump from ${SET_A_PRIMARY_HOST}\n" |tee -a ${LOG_FILE}	
	sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${SET_A_PRIMARY_HOST} sudo -u postgres  sh ${DB_DUMP_SCRIPT} ${DB_NAME}
		

        sshpass -p ${ID_PASS} scp ${USER_ID}@${SET_A_PRIMARY_HOST}:${COPY_DB_DUMP_FROM}/${DB_NAME}_*_${DB_DUMP_DATE}.* /tmp

        if [[ $? -ne 0 ]]; then
        printf "Failed: Not able to run following command: scp ${USER_ID}@${SET_A_PRIMARY_HOST}:${COPY_DB_DUMP_FROM}/${DB_NAME}_*_${DB_DUMP_DATE}.* /tmp\n" |tee -a ${LOG_FILE}
        exit 1
        fi

	
	for host_name in ${SET_A_OTHER_HOSTS}

	do 
	sshpass -p ${ID_PASS} scp /tmp/${DB_NAME}_*_${DB_DUMP_DATE}.* ${USER_ID}@${host_name}:/tmp

        if [[ $? -ne 0 ]]; then
        printf "Failed: Not able to run following command: scp /tmp/${DB_NAME}_*_${DB_DUMP_DATE}.* ${USER_ID}@${host_name}:/tmp\n" |tee -a ${LOG_FILE}
        exit 1
        fi

	done

	elif  [[ $ARG == "B" ]];then

	printf "Taking the DB dump from ${SET_B_PRIMARY_HOST}\n" |tee -a ${LOG_FILE}

        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${SET_B_PRIMARY_HOST} sudo -u postgres  sh ${DB_DUMP_SCRIPT} ${DB_NAME}

	
        sshpass -p ${ID_PASS} scp ${USER_ID}@${SET_B_PRIMARY_HOST}:${COPY_DB_DUMP_FROM}/${DB_NAME}_*_${DB_DUMP_DATE}.* /tmp
       	if [[ $? -ne 0 ]]; then
	        printf "Failed: Not able to run following command: scp ${USER_ID}@${SET_B_PRIMARY_HOST}:${COPY_DB_DUMP_FROM}/${DB_NAME}_*_${DB_DUMP_DATE}.* /tmp\n" |tee -a ${LOG_FILE}
        exit 1
        fi


        for host_name in ${SET_B_OTHER_HOSTS}

        do
       sshpass -p ${ID_PASS} scp /tmp/${DB_NAME}_*_${DB_DUMP_DATE}.* ${USER_ID}@${host_name}:/tmp

        if [[ $? -ne 0 ]]; then
        printf "Failed: Not able to run following command: scp /tmp/${DB_NAME}_*_${DB_DUMP_DATE}.* ${USER_ID}@${host_name}:/tmp\n" |tee -a ${LOG_FILE}
        exit 1
        fi


        done


	else 

		Printf  "Failed: Argument is Not the A and B\n" |tee -a ${LOG_FILE}

fi

} 

function bring_down_play_apps () {
        if [[ $ARG == "A" ]];then
	SERVERS=${SET_A_OTHER_HOSTS}
	elif [[ $ARG == "B" ]];then
	SERVERS=${SET_B_OTHER_HOSTS}
	else
		printf "Argument is not passed exiting from the script/n"
		exit 1
	fi 
        echo "I am here"
	for play_host in ${SERVERS}
	do 
		sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${play_host}<<EOF
	sudo su - play
	cd ${PLAY_APP_DIR}
	${PLAY_BIN} stop ${PLAY_APP_DIR}
	printf  "Cheking if Porcess still running:"
	sleep 3
	PROCESS_STATUS=$(/bin/ps -ef | grep "\b${PLAY_APP_NAME}\b" |grep -v grep)
	printf "Status of PLAY Process: ${PROCESS_STATUS}"
	if [[ "${PROCESS_STATUS}" == "" ]];
	then
        printf "Play application ${play_host} is down now"
	else
        PID=$(/bin/ps -ef | grep "\b${PLAY_APP_NAME}\b" |grep -v grep | awk ' { print $2 } ')
        /bin/kill -9 ${PID}
        PROCESS_STATUS=$(/bin/ps -ef | grep "\b${PLAY_APP_NAME}\b" |grep -v grep)
        printf "Status of PLAY Process: ${PROCESS_STATUS}"
        if [[ "${PROCESS_STATUS}" == "" ]];
        then
        printf "Play application ${play_host} is down now"
        else
                printf "Application has not been down please stop using the play stop and start menu when deployment complete "
                sleep 3
        fi
	fi
EOF
	done
}
function drop_db () {

        if [[ $ARG == "A" ]];then
        SERVERS=${SET_A_OTHER_HOSTS}
        elif [[ $ARG == "B" ]];then
        SERVERS=${SET_B_OTHER_HOSTS}
        else

                printf "Failed: Argument is not passed exiting from the script\n" | tee -a ${LOG_FILE}
                exit 1
        fi

        for host in ${SERVERS}
        do
	       sshpass -p ${ID_PASS} scp ${DB_DROP_SCRIPT} ${USER_ID}@${host}:/tmp

		printf "Dropping the DATABASE on ${host}\n" |tee -a ${LOG_FILE}

	        if [[ $? -ne 0 ]]; then
        	printf "Failed: Not able to run following command: scp ${DB_DROP_SCRIPT} ${USER_ID}@${host}:/tmp" |tee -a ${LOG_FILE}
       		 exit 1
        	fi

                sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${host}<<EOF

	        sudo su - postgres
		psql -f /tmp/${DB_DROP_SCRIPT_NAME} |tee -a ${LOG_FILE}
EOF

done

}


function create_db () {

        if [[ $ARG == "A" ]];then
        SERVERS=${SET_A_OTHER_HOSTS}
        elif [[ $ARG == "B" ]];then
        SERVERS=${SET_B_OTHER_HOSTS}
        else
                printf "Failed: Argument is not passed exiting from the script\n" | tee -a ${LOG_FILE}
		exit 1
        fi
        
        for host in ${SERVERS}
        do
               sshpass -p ${ID_PASS} scp ${DB_DROP_SCRIPT} ${USER_ID}@${host}:/tmp

                printf "Creating the DATABASE on ${host}\n" |tee -a ${LOG_FILE}

	        if [[ $? -ne 0 ]]; then
        	printf "Failed: Not able to run following command: scp ${DB_DROP_SCRIPT} ${USER_ID}@${host}:/tmp\n" |tee -a ${LOG_FILE}
        	exit 1
        	fi

                sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${host}<<EOF

                sudo su - postgres
		psql -f /tmp/${DB_NAME}_globals_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
                psql -f /tmp/${DB_NAME}_createdb_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
		psql -f /tmp/${DB_NAME}_extensions_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
		pg_restore -d ${DB_NAME} /tmp/${DB_NAME}_FULL_${DB_DUMP_DATE}.dump |tee -a ${LOG_FILE}
EOF

done

}

function bring_up_play_apps () {
        if [[ $ARG == "A" ]];then
        SERVERS=${SET_A_OTHER_HOSTS}
        elif [[ $ARG == "B" ]];then
        SERVERS=${SET_B_OTHER_HOSTS}
        else
                printf "Argument is not passed exiting from the script\n"
                exit 1
        fi
        echo "I am here"
        for play_host in ${SERVERS}
        do
                sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${play_host}<<EOF
	sudo su - play
        cd ${PLAY_APP_DIR}
	${PLAY_BIN} clean ${PLAY_APP_DIR}
	${PLAY_BIN} dependencies --sync
        ${PLAY_BIN} start ${PLAY_APP_DIR} &
		
        printf  "Cheking if Porcess has been start, and sleeping 30 seconds"
        sleep 15
	${PLAY_BIN} status ${PLAY_APP_DIR}
	exit
EOF
        done
}

function clean_up () {


        if [[ $ARG == "A" ]];then
        SERVERS=${SET_A_OTHER_HOSTS}
        elif [[ $ARG == "B" ]];then
        SERVERS=${SET_B_OTHER_HOSTS}
        else
                printf "Failed: Argument is not passed exiting from the script\n" | tee -a ${LOG_FILE}
                exit 1
        fi

        for host in ${SERVERS}
        do
		printf "Cleaning the files from tmp directory for: ${host}\n" 
	
               sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${host}<<EOF

                rm -f /tmp/${DB_NAME}_globals_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
                rm -f /tmp/${DB_NAME}_createdb_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
                rm -f /tmp/${DB_NAME}_extensions_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
                rm -f /tmp/${DB_NAME}_FULL_${DB_DUMP_DATE}.dump |tee -a ${LOG_FILE}
                rm -f /tmp/drop_db.sql

EOF

done

printf "Cleaning the tmp of local host:"  | tee -a ${LOG_FILE}

rm -f /tmp/${DB_NAME}_globals_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
rm -f /tmp/${DB_NAME}_createdb_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
rm -f /tmp/${DB_NAME}_extensions_${DB_DUMP_DATE}.sql |tee -a ${LOG_FILE}
rm -f /tmp/${DB_NAME}_FULL_${DB_DUMP_DATE}.dump |tee -a ${LOG_FILE}
rm -f /tmp/drop_db.sql

}

make_db_dump_and_scp

drop_db

create_db 

clean_up

Message="Log of dump_database_to_other_servers.sh script"

echo "$Message" | mailx -s "${Message}" pavan.kumar@eagletechva.com < ${LOG_FILE}



