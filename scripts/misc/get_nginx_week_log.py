#!/bin/python
import sys, paramiko
from datetime import timedelta, datetime, tzinfo, date
import getpass, re 
from scp import SCPClient
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--days', type=int, help="add the -d or --day argument with number of days")
#parser.parse_args()
args =  parser.parse_args()
print args.days


try:
    #hostname = raw_input('hostname: ')
    #username = raw_input('username: ')
    #password = getpass.getpass('password: ')
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect('bhprd4.eagletechva.com')
    #scp = client.open_sftp()
    scp = SCPClient(client.get_transport())

    week_days = args.days
    path = '/var/log/nginx'
    local_path = '/local/apps/bhsis/hit_htmls/access_files'
    today_date_with_dash = date.today()
    today_date_without_dash = re.sub('-', '', str(today_date_with_dash))
    today_access_zip_file = "access.log-%s.gz"  % (today_date_without_dash)
    today_from_loc = "%s/%s" % (path, today_access_zip_file)
    today_to_loc = "%s/%s" % (local_path, today_access_zip_file)
    scp.get(today_from_loc, today_to_loc)

    for week_day in range(1, week_days ):
        
         date_with_dash = date.today() - timedelta(week_day)
	 date_without_dash = re.sub('-', '', str(date_with_dash))
         print "date is now %s" % (date_without_dash)
         access_zip_file = "access.log-%s.gz"  % (date_without_dash)
	 from_loc = "%s/%s" % (path, access_zip_file)
	 print (from_loc)
	 to_loc = "%s/%s" % (local_path, access_zip_file)
	 print (to_loc)
	 scp.get(from_loc, to_loc)


finally:
    scp.close()
    client.close()


