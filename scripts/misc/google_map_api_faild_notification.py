#!/usr/bin/env python
##Created by Pavan Kumar to send out the email notication if Locator Google Map API kyes does not have status 'OK'

import urllib2
import json
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import re

## Autocomplete API (when users type their start address)
autocomplete_api = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:us&input=1901+North+Moore+Street+Arlington,+VA&key=AIzaSyDPni-q0MMWdPAGrlv7wS8AYgfmcGUo4as'

## Geocode API (to get lat/lng from address)
geocode_api='https://maps.googleapis.com/maps/api/geocode/json?address=1901+North+Moore+Street+Arlington,+VA&key=AIzaSyDPni-q0MMWdPAGrlv7wS8AYgfmcGUo4as'

## Directions API (to get directions from start address to facility)
direction_api='https://maps.googleapis.com/maps/api/directions/json?origin=38.8938808,-77.0715055&destination=38.8977995,-77.0710903&key=AIzaSyDPni-q0MMWdPAGrlv7wS8AYgfmcGUo4as'

fromaddr = 'pavan.kumar@eagletechva.com'
toaddrs = ['pavan.kumar@eagletechva.com', 'bhsis_infra@eagletechva.com' , 'ET_ALARMS@eagletechva.com', 'lisa.zhao@eagletechva.com', 'feng.jiang@eagletechva.com', 'jasraj.kaur@eagletechva.com']

##Send email notficatin
def email_notification(email_sub, email_body):
  msg = MIMEMultipart()
  msg = MIMEText(email_body, "html")

  msg['From'] = fromaddr
  msg['To'] = ", ".join(toaddrs)
  msg['Subject'] = "{}".format(email_sub)

  server = smtplib.SMTP('eagletechva-com.mail.protection.outlook.com')
  server.sendmail(fromaddr, toaddrs, msg.as_string())
  server.quit()
  
##Check the API STATUS
def api_url_status_check():
  api_urls = [ autocomplete_api, geocode_api, direction_api ]
  for api_url in api_urls:
    req = urllib2.Request(api_url)
    try:
      response = urllib2.urlopen(req)
    except urllib2.HTTPError as e:
      print 'The server couldn\'t fulfill the request.'
      email_body = "<p>Production Locator API_URL: {0}, couldn\'t fulfill the request.</p> \
                    <p>HTTP Error Code is:<strong> {1}</strong></p>".format(api_url, e.code)
      email_sub = "FAILED: PROD Locator Google API URL STATUS is not 200: {}".format(e.code)
      email_notification(email_sub, email_body)
    except urllib2.URLError as e:
      print 'We failed to reach a server.'
      email_body = "<p>Production Locator: <strong>Could not reach</strong> to API_URL: {0}.</p> \
                    <p> Reason: <strong>{1}</strong></p>".format(api_url, e.reason)
      email_sub = "FAILED: PROD Locator Google API URL has connection issues: {}".format(e.reason)
      email_notification(email_sub, email_body)
      print 'Reason: ', e.reason
    else:
      api_response = response.read()
      api_json = json.loads(api_response)
      if api_json['status'] != 'OK':
        print(api_json['status'])
        email_body = "<p><strong style='color:red;'> FAILED: </strong> Production Locator Google Map API URL Status is \
                      <strong style='color:red;'> Not 'OK' </strong>. STATUS:  <strong style='color:red;'>{0}</strong>. \
                      </p><p> API URL: {1} </p>".format(api_json['status'], api_url)
        email_sub = "FAILED: PROD Locator Google Map API Status is Not 'OK', Status is {}".format(api_json['status'])
        email_notification(email_sub, email_body)
      else:
        print(api_json['status'])


##Calling the function
api_url_status_check()
