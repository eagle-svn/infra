#!/bin/sh
##Created By Pavan Kumar
#this script send the error the log message to users via email


Properties_File="/local/apps/bhsis/scripts/ibhs/system_log.properties"

. ${Properties_File}


function log_count_and_time_reset () {


if [[ ! -z "${Error_Out_Put}" ]]

then
        Log_Message_Count=`expr ${Log_Message_Count} + ${Log_Count}`

        From_Properties_Last_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`

	sed -i -e "s/^Log_Message_Count=${From_Properties_Last_Message_Count}/Log_Message_Count=${Log_Message_Count}/" ${Properties_File}

        Message_Time=`expr ${Message_Time} + ${Sleep_Sec}`

        From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=${Message_Time}/" ${Properties_File}

        Hour_Reset=`expr ${Hour_Reset} - ${Sleep_Sec}`

        From_Properties_Hour_Reset=`grep "^Hour_Reset" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Hour_Reset=${From_Properties_Hour_Reset}/Hour_Reset=${Hour_Reset}/" ${Properties_File}

else

        Message_Time=`expr ${Message_Time} + ${Sleep_Sec}`

        From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=${Message_Time}/" ${Properties_File}

	if [[ ${Message_Time} -gt ${Total_Message_Time} ]]
	then

        	From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`

        	sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=1/" ${Properties_File}
        
		From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`

        	sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=1/" ${Properties_File}

        	From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`

        	sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=1/" ${Properties_File}

	fi


       	Hour_Reset=`expr ${Hour_Reset} - ${Sleep_Sec}`

        From_Properties_Hour_Reset=`grep "^Hour_Reset" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Hour_Reset=${From_Properties_Hour_Reset}/Hour_Reset=${Hour_Reset}/" ${Properties_File}


	if [[ ${Hour_Reset} -le 60 ]]

	then

        	From_Properties_Hour_Reset=`grep "^Hour_Reset" ${Properties_File} |awk -F= '{print $2}'`


        	if [[ ${Email_Send_Flag} == "FALSE" ]]
        	then

                	sed -i -e "s/^Email_Send_Flag=FALSE/Email_Send_Flag=TRUE/" ${Properties_File}

        	else
        		:
        	fi


        sed -i -e "s/^Hour_Reset=${From_Properties_Hour_Reset}/Hour_Reset=${Total_Hours}/" ${Properties_File}

        From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=1/" ${Properties_File}

        From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=1/" ${Properties_File}


	fi



fi

}


function get_the_log_string () {


Last_Error_Line_Num=`grep -n "${Error_Message_TO_Check}" ${LOG_File_Name} | awk -F: '{print $1}' | tail  -1`

From_Properties_Last_Error_Line_Num=`grep "^Last_Error_Line_Num" ${Properties_File} |awk -F= '{print $2}'`


if [[ ${Last_Error_Line_Num} -eq ${From_Properties_Last_Error_Line_Num} ]] 
then

	log_count_and_time_reset
	
	cat ${Properties_File}

	echo "Error page: ${Error_Out_Put}"

	exit 1


else

	log_count_and_time_reset

fi


Error_Print_Range_Num=`expr ${Last_Error_Line_Num} + ${Print_Line_No}`

RE_Check_Error_Line_Num=${Error_Print_Range_Num}


echo "RE_Check_Error_Line_Num $RE_Check_Error_Line_Num"

From_Properties_RE_Check_Error_Line_Num=`grep "^RE_Check_Error_Line_Num" ${Properties_File}`

echo "From_Properties_RE_Check_Error_Line_Num $From_Properties_RE_Check_Error_Line_Num"

sed -i -e "s/${From_Properties_RE_Check_Error_Line_Num}/RE_Check_Error_Line_Num=${RE_Check_Error_Line_Num}/g" ${Properties_File}

echo "sed -i -e "s/${From_Properties_RE_Check_Error_Line_Num}/RE_Check_Error_Line_Num=${RE_Check_Error_Line_Num}/g" ${Properties_File}"


From_Properties_Last_Error_Line_Num2=`grep "^Last_Error_Line_Num" ${Properties_File}`

echo "From_Properties_Last_Error_Line_Num2 $From_Properties_Last_Error_Line_Num2"

sed -i -e "s/${From_Properties_Last_Error_Line_Num2}/Last_Error_Line_Num=${Last_Error_Line_Num}/g" ${Properties_File}


Error_Out_Put=`sed -n -e "${Last_Error_Line_Num},${Error_Print_Range_Num}p"  ${LOG_File_Name}`



}


function send_email () {


message=`echo "${Email_Message}


Log Message:

${Error_Out_Put}"`


echo "${message}" | mail -s "${Email_Subject}"  ${Email_Address}



}

function main () {

if  [ ${Log_Message_Count} -le  ${Total_Log_Count} ] && [ ${Message_Time}  -le ${Total_Message_Time} ]
then
	get_the_log_string
        log_count_and_time_reset


fi

if [ ${Log_Message_Count} -ge  ${Total_Log_Count} ] && [ ${Message_Time}  -le ${Total_Message_Time} ]
then


	sed -i -e "s/^Email_Send_Flag=TRUE/Email_Send_Flag=FALSE/" ${Properties_File}

	log_count_and_time_reset

else

	log_count_and_time_reset
 
fi 


if [[ ${Email_Send_Flag} == "TRUE" ]] && [[ ! -z "${Error_Out_Put}" ]]
then

	send_email
	
	log_count_and_time_reset
else

	log_count_and_time_reset

fi

if [[ ${Log_Message_Count} -gt ${Total_Log_Count} ]]
then

	From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`

	sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=1/" ${Properties_File}

fi

if [[ ${Message_Time} -gt ${Total_Message_Time} ]]
then

	From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=1/" ${Properties_File}

	From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`

	sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=1/" ${Properties_File}


fi



if [[ ${Hour_Reset} -le 60 ]]

then

        From_Properties_Hour_Reset=`grep "^Hour_Reset" ${Properties_File} |awk -F= '{print $2}'`

        if [[ ${Email_Send_Flag} == "FALSE" ]]
        then

                sed -i -e "s/^Email_Send_Flag=FALSE/Email_Send_Flag=TRUE/" ${Properties_File}

        else
        :
        fi

        sed -i -e "s/^Hour_Reset=${From_Properties_Hour_Reset}/Hour_Reset=${Total_Hours}/" ${Properties_File}

	From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`

	sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=1/" ${Properties_File}
	
        From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`

        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=1/" ${Properties_File}


fi

}

main

cat ${Properties_File}

echo "Error page: ${Error_Out_Put}"
