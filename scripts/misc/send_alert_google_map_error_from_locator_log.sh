#!/bin/sh
##Created By Pavan Kumar
#this script send the error the log message to users via email
#This script bound the the second varialbe which can be made dynamic by arguments and while loop. 
##Curently second varialbe is hard corded, please look at the property file. 
Properties_File="/local/apps/bhsis/scripts/loc/google_alert_log.properties"
. ${Properties_File}

function log_count_and_time_reset () {
if [[ ! -z "${Error_Out_Put}" ]]
then
 echo "I am coming here 12"
        echo "ERROR in IF out put: ${Error_Out_Put}" 
        Log_Message_Count=`expr ${Log_Message_Count} + ${Log_Count}`
        From_Properties_Last_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`
	sed -i -e "s/^Log_Message_Count=${From_Properties_Last_Message_Count}/Log_Message_Count=${Log_Message_Count}/" ${Properties_File}
        Message_Time=`expr ${Message_Time} + ${Sleep_Sec}`
        From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`
        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=${Message_Time}/" ${Properties_File}
        Hour_Reset=`expr ${Hour_Reset} - ${Sleep_Sec}`
        From_Properties_Hour_Reset=`grep "^Hour_Reset" ${Properties_File} |awk -F= '{print $2}'`
        sed -i -e "s/^Hour_Reset=${From_Properties_Hour_Reset}/Hour_Reset=${Hour_Reset}/" ${Properties_File}
else
	echo "I am coming here 13"
  	echo "ERROR in else out put: ${Error_Out_Put}" 
  	From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`
        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=0/" ${Properties_File}
	From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`
        sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=0/" ${Properties_File}
fi
}

function get_the_log_string () {
Last_Error_Line_Num=`grep -n "${Error_Message_TO_Check}" ${LOG_File_Name} | awk -F: '{print $1}' | tail  -1`
From_Properties_Last_Error_Line_Num=`grep "^Last_Error_Line_Num" ${Properties_File} |awk -F= '{print $2}'`
echo ""
echo "${Last_Error_Line_Num} -eq ${From_Properties_Last_Error_Line_Num}"

echo ""
if [[ ${Last_Error_Line_Num} -eq ${From_Properties_Last_Error_Line_Num} ]] 
then
	log_count_and_time_reset
	cat ${Properties_File}
	echo "My Error page: ${Error_Out_Put}"
        exit 1	
else
 :
fi
Error_Print_Range_Num=`expr ${Last_Error_Line_Num} + ${Print_Line_No}`
RE_Check_Error_Line_Num=${Error_Print_Range_Num}
echo "RE_Check_Error_Line_Num $RE_Check_Error_Line_Num"
From_Properties_RE_Check_Error_Line_Num=`grep "^RE_Check_Error_Line_Num" ${Properties_File}`
echo "From_Properties_RE_Check_Error_Line_Num $From_Properties_RE_Check_Error_Line_Num"
sed -i -e "s/${From_Properties_RE_Check_Error_Line_Num}/RE_Check_Error_Line_Num=${RE_Check_Error_Line_Num}/g" ${Properties_File}
echo "sed -i -e "s/${From_Properties_RE_Check_Error_Line_Num}/RE_Check_Error_Line_Num=${RE_Check_Error_Line_Num}/g" ${Properties_File}"

From_Properties_Last_Error_Line_Num2=`grep "^Last_Error_Line_Num" ${Properties_File}`
echo "From_Properties_Last_Error_Line_Num2 $From_Properties_Last_Error_Line_Num2"
sed -i -e "s/${From_Properties_Last_Error_Line_Num2}/Last_Error_Line_Num=${Last_Error_Line_Num}/g" ${Properties_File}
Error_Out_Put=`sed -n -e "${Last_Error_Line_Num},${Error_Print_Range_Num}p"  ${LOG_File_Name}`

if [[ ! -z "${Error_Out_Put}" ]]
then
log_count_and_time_reset
fi
}

function send_email () {
message=`echo "${Email_Message}
Log Message:
${Error_Out_Put}"`
echo "${message}" | mail -s "${Email_Subject}"  ${Email_Address}

}

function main () {

if [ ${Log_Message_Count} -ge  ${Total_Log_Count} ] && [ ${Message_Time}  -ge ${Total_Message_Time} ]
then
	sed -i -e "s/^Email_Send_Flag=FALSE/Email_Send_Flag=TRUE/" ${Properties_File}
        send_email
	
        get_the_log_string
else
        get_the_log_string
fi 


if [[ ${Log_Message_Count} -gt ${Total_Log_Count} ]]
then
	From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`
	sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=1/" ${Properties_File}
fi

if [[ ${Message_Time} -gt ${Total_Message_Time} ]]
then
	From_Properties_Message_Time=`grep "^Message_Time" ${Properties_File} |awk -F= '{print $2}'`
        sed -i -e "s/^Message_Time=${From_Properties_Message_Time}/Message_Time=0/" ${Properties_File}
	From_Properties_Log_Message_Count=`grep "^Log_Message_Count" ${Properties_File} |awk -F= '{print $2}'`
	sed -i -e "s/^Log_Message_Count=${From_Properties_Log_Message_Count}/Log_Message_Count=0/" ${Properties_File}
fi

}

main
cat ${Properties_File}
echo "Error page: ${Error_Out_Put}"
