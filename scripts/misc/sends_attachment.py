import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
from datetime import timedelta, datetime, tzinfo, date
 
fromaddr = "pavan.kumar@eagletechva.com"
toaddr = "pavan.kumar@eagletechva.com, michael.dymek@eagletechva.com, bhsis_infra@eagletechva.com"
#toaddr = "bhsis_infra@eagletechva.com"

msg = MIMEMultipart()
 
msg['From'] = fromaddr
msg['To'] = toaddr
msg['Subject'] = "DASIS Home Page Hit Counts"
 
body = "DASIS Home Page Hit Counts, please find the attached file. Forward this email to Michael Dymek if he does not receive the email or if he asks the DASIS Home Page Hit Counts."
 
msg.attach(MIMEText(body, 'plain'))


today_date = date.today()
html_file_loc = '/local/apps/bhsis/hit_htmls'
filename = "hits_count_%s.html" % today_date
attachment = open("%s/%s" % (html_file_loc, filename), "rb")
 
part = MIMEBase('application', 'octet-stream')
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
 
msg.attach(part)
 
server = smtplib.SMTP('eagletechva-com.mail.protection.outlook.com')
#server.starttls()
#server.login(fromaddr, "YOUR PASSWORD")
text = msg.as_string()
server.sendmail(fromaddr, toaddr, text)
server.quit()
