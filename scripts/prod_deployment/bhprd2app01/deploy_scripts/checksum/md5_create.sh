#!/bin/bash
## Created by Pavan Kumar
## Purpose of this script is to check the md5sum has value of files. and exclude the file you don't want to checksum. 



usage()
{
cat << EOF
usage: $0 options

This script run with these parameters

OPTIONS:
        
        -d Directory path to check md5sum hash
        -e exclude the file that you do not want to checksum
        -r directory path where you want to save the result
        -h help message to use this script
EOF
}


MD5_DIR=
MD5_EXCLUDE_DIR=
MD5_RESULT_DIR=

if [[ $# -eq "" ]]

then

usage

exit 1

else
:

fi

while getopts "hd:d:e:r:" OPTION

do

case $OPTION in
        h) usage
                exit 1 ;;

 

        d) MD5_DIR=$OPTARG
        ;;

        e) MD5_EXCLUDE_DIR=$OPTARG
        ;;
        r) MD5_RESULT_DIR=$OPTARG
        ;;

        ?)
        usage
        exit
        ;;
esac

done

if [[ ! -d  ${MD5_DIR}  ]]
then
tput setf 4
echo "Directory Path ${MD5_DIR} does not exist"
tput sgr 0
exit 1

else
:

fi

if [[ ! -d  ${MD5_RESULT_DIR}  ]]
then
tput setf 4
echo "Directory Path ${MD5_RESULT_DIR} does not exist"
echo "Creating the directory name ${MD5_RESULT_DIR}"

mkdir -p ${MD5_RESULT_DIR}
tput sgr 0

exit 1

else
:

fi


MD5_RESULT_FILE=${MD5_RESULT_DIR}/md5_result.txt

if [[ -f  ${MD5_RESULT_FILE}  ]]
then
tput setf 4
echo "File exist removing the file"
tput sgr 0
/bin/rm -f ${MD5_RESULT_FILE}

else
:

fi
 
sleep 5 

cd ${MD5_DIR}

/usr/bin/find  -type f ! -iname "${MD5_EXCLUDE_DIR}" -exec md5sum "{}" + > ${MD5_RESULT_FILE}

#cat ${MD5_RESULT_FILE}
