#!/bin/bash
## Created by Pavan Kumar
## Purpose of this script is to check the md5sum has value of files. and exclude the file you don't want to checksum. 


usage()
{
cat << EOF
usage: $0 options

This script run with these parameters

OPTIONS:
        
        -d Directory path to check md5sum hash
        -e Give the file name with path where you want to save the result: default is /tmp/md5sum_verify_result.txt
        -r directory path where is the result file. 
	-p property_file for email variables
        -h help message to use this script
EOF
}


MD5_DIR=
MD5_VERIFY_FILE=
MD5_RESULT_FILE=
PROJECT_PROPERTY_FILE=

if [[ $# -eq "" ]]

then

usage

exit 1

else
:

fi

while getopts "hd:d:r:e:p:" OPTION

do

case $OPTION in
        h) usage
                exit 1 ;;

 

        d) MD5_DIR=$OPTARG
        ;;

        r) MD5_RESULT_FILE=$OPTARG
        ;;
        e) MD5_VERIFY_FILE=$OPTARG 
        ;;
        p) PROJECT_PROPERTY_FILE=$OPTARG
        ;;

	 ?)
        usage
        exit
        ;;
esac

done

if [[ ! -d  ${MD5_DIR}  ]]
then
tput setf 4
echo "Directory Path ${MD5_DIR} does not exist"
tput sgr 0
exit 1

else
:

fi

if [[ ! -f  ${MD5_RESULT_FILE}  ]]
then
tput setf 4
echo "MD5 Result File ${MD5_RESULT_FILE} does not exist"
tput sgr 0
exit 1

else
:

fi

if [[ ! -f  ${PROJECT_PROPERTY_FILE}  ]]
then
tput setf 4
echo "Property File ${PROJECT_PROPERTY_FILE} does not exist"
tput sgr 0
exit 1

else
. ${PROJECT_PROPERTY_FILE}

fi

if [[ ${MD5_VERIFY_FILE} == "" ]] 
then
  MD5_VERIFY_FILE=/tmp/md5sum_verify_result.txt

else
:

fi



if [[ -f ${MD5_VERIFY_FILE}  ]]
then

echo "Removing the file ${MD5_VERIFY_FILE}"

/bin/rm -f ${MD5_VERIFY_FILE}

else
:

fi


cd ${MD5_DIR}

md5sum --quiet --check ${MD5_DIR} ${MD5_RESULT_FILE}  |  tee -a ${MD5_VERIFY_FILE}

if [[ "$?" -ne "0" ]]
then
 
echo "FAILED: Command md5sum --check ${MD5_DIR} ${MD5_RESULT_FILE} |tee -a ${MD5_VERIFY_FILE} failed"

else

: 

fi 

RESULT=$(cat ${MD5_VERIFY_FILE} |grep -i "FAILED" |grep -v grep )




if [[ "$?" -eq "0" ]]
then

MESSAGE=$(echo "${email_sub}



$RESULT")

else

MESSAGE=$(echo "${email_sub}

CheckSUM Result is OK. 

$RESULT")


fi

echo "$MESSAGE "
echo "${MESSAGE}" | mailx -s "$email_sub $project_name" ${email_addrs}
