#!/bin/sh
##Created by Pavan Kumar
##This script deploy the changes for play application.

BASE_DIR="/local/apps/bhsis/deploy_scripts"

function usage () {

echo "Project Names are: ibhs, brc, aims, auth_service and cron-brc, cron-aims, cron-ibhs"
echo ""
echo ""
}
usage

DATE=`date +%m%d%y_%H%M%S`



function play_app_deployment () {
. ${BASE_DIR}/projects/${project_name}/${project_name}_env.properties

VAR_PROPERTY_FILE="${BASE_DIR}/projects/${project_name}/.${project_name}_var.properties"


if [[ -f $VAR_PROPERTY_FILE ]]
then
:

else

echo "File does not exist $VAR_PROPERTY_FILE"
exit 1
fi

REPLACE_VAR_SCRIPT="replace_variable.sh"

printf "Please Enter Relese Version:"
read REL_VERSION

CHECK_REL_VERSION=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')
REL_VER_VERIFICATION=`echo "$CHECK_REL_VERSION" | grep "\." | sed  's/\.//g' | grep "^[0-9]\{1,5\}$"`

if [ "${REL_VER_VERIFICATION}" == "" ]
then

echo "Release number format is incorrect, it must me like: 2.3.4"
echo "Exiting from the deployment script  in 2 seconds"

exit 1

sleep 2

else

:

fi


APP_CONF_FILE="${play_deploy_path}/${project_name}_${REL_VERSION}/conf/application.conf"


printf "Please Enter SVN Revision number to copy  TAG(Hit Enter if you want to use Head Revision):"
#read -s -n 1 REVISION
read  REVISION
echo "Revision number is: $REVISION"

if [[ "${REVISION}" == "" ]]
then
      export EXPORT_REVISION="HEAD"

else
	expr ${REVISION} + 1

	if [[ "$?" -ne 0 ]]
	then

		export  EXPORT_REVISION=${REVISION}
 	else

		echo "Revision number has not been enter correctly"

		exit 1

	fi

fi    



##Stopping the Play application.

${play_bin} stop ${play_app_dir}

##Checking if the process is still running. 

echo "Cheking if Porcess still running:"
sleep 3
PROCESS_STATUS=`/bin/ps -ef | grep "${play_app_name}" |grep -v grep`

echo "Status of PLAY Process: ${PROCESS_STATUS}"

if [[ "${PROCESS_STATUS}" == "" ]];
then


echo "Play application ${play_host} is down now"

else 

PROCESS_PID=`/bin/ps -ef | grep "${play_pid}" |grep -v grep | awk '{ print $1 }'`

/bin/kill -9 ${PROCESS_PID}

fi



if [[ -d ${play_deploy_path}/${project_name}_${REL_VERSION} ]]

then 

/bin/mv ${play_deploy_path}/${project_name}_${REL_VERSION} ${play_deploy_path}/${project_name}_${REL_VERSION}_${DATE}

else

:

fi


svn list -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} --username ${svn_user} --password ${svn_password}

if [[ "$?" -eq 0 ]]
then
	:

else

echo "Failed: Tag ${svn_export_url}/${project_name}_${REL_VERSION} does not exist on SVN, please check"

exit 1

fi
## SVN export of the release number:

svn export -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} ${play_deploy_path}/${project_name}_${REL_VERSION}  --username ${svn_user} --password ${svn_password}

if [[ "$?" -eq 0 ]]
then
        :

else

echo "Failed: export command 'svn export -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} ${play_deploy_path}/${project_name}_${REL_VERSION}' has failed please check"

exit 1

fi

##Replaing the variables value on the project tag directory


${BASE_DIR}/${REPLACE_VAR_SCRIPT} -p ${VAR_PROPERTY_FILE} -f ${APP_CONF_FILE} 


##Creating the link between project name and release number.

/bin/rm -f ${play_app_dir}

/bin/ln -s ${play_deploy_path}/${project_name}_${REL_VERSION} ${play_app_dir}

if [[ "$?" -eq 0 ]]
then
        :

else

echo "Failed: Not able to create the symbolik link between ${play_deploy_path}/${project_name}_${REL_VERSION} ${play_app_dir}, please check"

exit 1

fi

##Starting the play application server

#. ${play_bin}  dependencies --sync  ${play_app_dir}
cd ${play_app_dir}
 play dependencies --sync
play start ${play_app_dir}

echo "sleeping for 5 seconds"


echo "Making the CheckSUM values for the ${project_name}"

${md5_create_sh} -d ${play_app_dir} -e "${md5_exclude_value}" -r "${md5_result_file}"


echo "Deployment has been completed"

}



function cron_app_deployment () {




. ${BASE_DIR}/projects/${project_name}/${project_name}_env.properties

VAR_PROPERTY_FILE="${BASE_DIR}/projects/${project_name}/.${project_name}_var.properties"


if [[ -f $VAR_PROPERTY_FILE ]]
then
:

else

echo "File does not exist $VAR_PROPERTY_FILE"
exit 1
fi

REPLACE_VAR_SCRIPT="replace_variable.sh"

printf "Please Enter Relese Version:"
read REL_VERSION

CHECK_REL_VERSION=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')
REL_VER_VERIFICATION=`echo "$CHECK_REL_VERSION" | grep "\." | sed  's/\.//g' | grep "^[0-9]\{1,5\}$"`

if [ "${REL_VER_VERIFICATION}" == "" ]
then

echo "Release number format is incorrect, it must me like: 2.3.4"
echo "Exiting from the deployment script  in 2 seconds"

exit 1

sleep 2

else

:

fi



printf "Please Enter SVN Revision number to copy  TAG(Hit Enter if you want to use Head Revision):"
#read -s -n 1 REVISION
read  REVISION
echo "Revision number is: $REVISION"

if [[ "${REVISION}" == "" ]]
then
      export EXPORT_REVISION="HEAD"

else
        expr ${REVISION} + 1

        if [[ "$?" -ne 0 ]]
        then

                export  EXPORT_REVISION=${REVISION}
        else

                echo "Revision number has not been enter correctly"

                exit 1

        fi

fi



if [[ -d ${cron_deploy_path}/${project_name}_${REL_VERSION} ]]

then

/bin/mv ${cron_deploy_path}/${project_name}_${REL_VERSION} ${cron_deploy_path}/${project_name}_${REL_VERSION}_${DATE}

else

:

fi


svn list -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} --username ${svn_user} --password ${svn_password}

if [[ "$?" -eq 0 ]]
then
        :

else

echo "Failed: Tag ${svn_export_url}/${project_name}_${REL_VERSION} does not exist on SVN, please check"

exit 1

fi
## SVN export of the release number:

svn export -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} ${cron_deploy_path}/${project_name}_${REL_VERSION}  --username ${svn_user} --password ${svn_password}

if [[ "$?" -eq 0 ]]
then
        :

else

echo "Failed: export command 'svn export -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} ${cron_deploy_path}/${project_name}_${REL_VERSION}' has failed please check"

exit 1

fi

##Replaing the variable value on the project tag directory

echo "Replacing the Token Name with its values"

${BASE_DIR}/${REPLACE_VAR_SCRIPT} -p ${VAR_PROPERTY_FILE} -d ${cron_deploy_path}/${project_name}_${REL_VERSION}

echo "Changing the permission of script to execute permission"

/bin/chmod -R 750 ${cron_deploy_path}/${project_name}_${REL_VERSION}

##Creating the link between project name and release number.

/bin/rm -f ${cron_app_dir}

/bin/ln -s ${cron_deploy_path}/${project_name}_${REL_VERSION} ${cron_app_dir}

if [[ "$?" -eq 0 ]]
then
        :

else

echo "Failed: Not able to create the symbolik link between ${cron_deploy_path}/${project_name}_${REL_VERSION} ${cron_app_dir}, please check"

exit 1

fi

echo "Deployment for ${project_name}_${REL_VERSION} has been completed"

}

echo -n "Enter the Project Name:"
read -r project_name

if [[ ${project_name} == "ibhs" ]] || [[ ${project_name} == "aims" ]] || [[ ${project_name} == "brc" ]] || [[ ${project_name} == "auth_service" ]] 
then

	echo -n "Do you want to run the deployment for  ${project_name}, Enter YES to continue else enter N:"
	read -r dp_ans

		
	if [[ "${dp_ans}" == "YES" ]]
	then
		play_app_deployment

	else
		echo "Deployment is failed, please run the deployment again"
		exit 1
	fi

elif [[  ${project_name} == "cron-brc" ]] || [[ ${project_name} == "cron-aims" ]] || [[ ${project_name} == "cron-ibhs" ]]
then

        echo -n "Do you want to run the deployment for  ${project_name}, Enter 'YES' to continue else enter 'N':"
        read -r dp_ans

        
        if [[ "${dp_ans}" == "YES" ]]
        then
                cron_app_deployment

        else
                echo "Deployment is failed, please run the deployment again"
                exit 1
        fi


else

echo "You have entered the wrong project name"

exit 1

fi

