#!/bin/sh
#Created by Pavan Kumar

BUILD_NUMBER=0

DATE=`date +%m%d%y_%H%M%S`
tput setf 5

if [ -d  ${play_main_log_dir}/${project_name} ] ;

then
	:

else
	echo "${play_main_log_dir}/${project_name} directory does not exist, Creating the Directory"

	mkdir ${play_main_log_dir}/${project_name}

fi

printf "Date and Time: $DATE \n" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

if [ "$?" -eq 0 ];
then
	:
else
	printf "Not able to create the log file: ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log\n"

fi

printf "${project_name} Major and Minor Branch Version number: ${branch_version}\n"|tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

printf "${project_name} Current Branch : ${svn_branch}\n" |tee -a  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

##?Branch Update need or not
### Version Update
printf "${project_name} Previous Release Version Number:${rel_version}\n" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log
tput sgr0
tput setf 2
printf "Do you want to update ${project_name} Release Version Number (Y, y or N, n):"

read ANS


if [ "${ANS}" == "Y" ] || [ "${ANS}" == "y" ] || [ "${ANS}" == "N" ] || [ "${ANS}" == "n" ]
then

	:


else

	echo "Please only Enter Y, y, N or n "
	sleep 1

	echo "Exiting from Menu, call menu again for Play Deploy"

	exit 1
fi


if [ "${ANS}" == "Y" ] || [ "${ANS}" == "y" ]
then
	printf "Please Enter Relese Version:"
	read REL_VERSION

	CHECK_REL_VERSION=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')
	REL_VER_VERIFICATION=`echo "$CHECK_REL_VERSION" | grep "\." | sed  's/\.//g' | grep "^[0-9]\{1,5\}$"`

	if [ "${REL_VER_VERIFICATION}" == "" ]
	then

		echo "Release number format is incorrect, it must me like: 2.3.4"
		echo "existing from the build menu in 2 seconds"

		exit 1

		sleep 2

	else

		:

	fi

	REL_VERSION_WITOUT_BUILD_NO=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')

	#Adding the Build number to the release number
	

	PRE_REL_VERSION=`grep "^rel_version" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

	PRE_REL_VERSION_WITH_BUILD_NO=`echo "${PRE_REL_VERSION}" | awk -F= '{print $2}'`

	PRE_REL_VERSION_WITHOUT_BUILD_NO=`echo "${PRE_REL_VERSION_WITH_BUILD_NO}" | awk -F_b '{ print $1 }'`

	if [[ "${PRE_REL_VERSION_WITHOUT_BUILD_NO}" == "${REL_VERSION_WITOUT_BUILD_NO}" ]]
	then
		

		printf "To increament the build number type(Y or y) to Continue with same number type(C or c), or type any key to start b0: "

		read USER_ANS

        	if [ "${USER_ANS}" == "Y" ] || [ "${USER_ANS}" == "y" ]

		then 

			
			BUILD_NUMBER=$(echo "${REL_VERSION}" | awk -F_b '{ print $2 }')
			#BUILD_NUMBER=$(echo "${PRE_REL_VERSION_WITH_BUILD_NO}" | awk -F_b '{ print $2 }')

			BUILD_NUMBER=$((BUILD_NUMBER + 1))

			#REL_VERSION=${PRE_REL_VERSION_WITHOUT_BUILD_NO}_b${BUILD_NUMBER}
			REL_VERSION=${REL_VERSION_WITOUT_BUILD_NO}_b${BUILD_NUMBER}

			echo ""
			echo ""

			/bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

	        elif [ "${USER_ANS}" == "C" ] || [ "${USER_ANS}" == "c" ]
		
		then
			/bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

		else 

			
			REL_VERSION_WITOUT_BUILD_NO=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')
			REL_VERSION=${REL_VERSION_WITOUT_BUILD_NO}_b${BUILD_NUMBER}


			PRE_REL_VERSION=`grep "^rel_version" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

			echo " Previous Release Version $PRE_REL_VERSION"

			/bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

		fi

	
	elif [[ "${PRE_REL_VERSION_WITHOUT_BUILD_NO}" != "${REL_VERSION_WITOUT_BUILD_NO}" ]]
        
	then
		printf "To increament the build number type(Y or y) to Continue with same number type(C or c), or type any key to start b0: "

                read USER_ANS

                if [ "${USER_ANS}" == "Y" ] || [ "${USER_ANS}" == "y" ]

                then


                        BUILD_NUMBER=$(echo "${REL_VERSION}" | awk -F_b '{ print $2 }')
                        #BUILD_NUMBER=$(echo "${PRE_REL_VERSION_WITH_BUILD_NO}" | awk -F_b '{ print $2 }')

                        BUILD_NUMBER=$((BUILD_NUMBER + 1))

                        #REL_VERSION=${PRE_REL_VERSION_WITHOUT_BUILD_NO}_b${BUILD_NUMBER}
                        REL_VERSION=${REL_VERSION_WITOUT_BUILD_NO}_b${BUILD_NUMBER}

                        echo ""
                        echo ""

                        /bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

                elif [ "${USER_ANS}" == "C" ] || [ "${USER_ANS}" == "c" ]

                then
                        /bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

                else

                        REL_VERSION_WITOUT_BUILD_NO=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')
                        REL_VERSION=${REL_VERSION_WITOUT_BUILD_NO}_b${BUILD_NUMBER}


                        PRE_REL_VERSION=`grep "^rel_version" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

                        echo " Previous Release Version $PRE_REL_VERSION"

                        /bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

                fi

		



	else
                REL_VERSION=${REL_VERSION_WITOUT_BUILD_NO}_b${BUILD_NUMBER}
	        PRE_REL_VERSION=`grep "^rel_version" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`
                echo " Previous Release Version $PRE_REL_VERSION"
                /bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties
	

	fi

	
fi

if [ "${ANS}" == "N" ] || [ "${ANS}" == "n" ]
then

printf "Do you want to continue with Same Version number (Y or N): "
read ANS2

if [ "${ANS2}" == "Y" ] || [ "${ANS2}" == "y" ]
then

	printf "If you want to increment the Build Number, then type(Y, y), else enter (n): "


        read USER_ANS

        if [ "${USER_ANS}" == "Y" ] || [ "${USER_ANS}" == "y" ]


	then
		echo ""
		printf "Increment the Build Number\n"
		echo ""

		PRE_REL_VERSION=`grep "^rel_version" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

		echo " Previous Release Version $PRE_REL_VERSION"

		PRE_REL_VERSION_WITH_BUILD_NO=`echo "${PRE_REL_VERSION}" | awk -F= '{print $2}'`

		BUILD_NUMBER=$(echo "${PRE_REL_VERSION_WITH_BUILD_NO}" | awk -F_b '{ print $2 }')

		PRE_REL_VERSION_WITHOUT_BUILD_NO=$(echo "${PRE_REL_VERSION_WITH_BUILD_NO}" | awk -F_b '{ print $1 }')

		BUILD_NUMBER=$((BUILD_NUMBER + 1))

		REL_VERSION=${PRE_REL_VERSION_WITHOUT_BUILD_NO}_b${BUILD_NUMBER}

		echo ""
		echo ""


		/bin/sed -i 's/'${PRE_REL_VERSION}'/rel_version='${REL_VERSION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties
	else
		:
	fi

else
printf "Exiting from the Build Menu in 2 sec\n"

sleep 2

exit 0

fi

fi

if [ "$?" -eq 0 ];
then
:
else
tput sgr0
tput setf 4
printf "Failed: Not able to Change the Version number in ${project_name}" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log
tput sgr0
fi

## Reseting the environment variale for project

. ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

tput sgr0
### Project SVN Revision Information:
tput setf 2
echo ""
printf "Please Enter SVN Revision number for BUILD and TAG(Hit Enter if you want to use Head Revision):"
read REVISION

expr ${REVISION} + 1

if [[ "$?" -ne 0 ]]
then

printf  "Please Renter SVN Revision value(Hit Enter if you want to use Head Revision):"
read REVISION

expr ${REVISION} + 1
   if [[ "$?" -ne 0 ]]
     then
tput sgr0
tput setf 4
 printf "Build Failed: Exiting from the BUILD MENU"
 tput sgr0
exit 1

 else
svn cleanup ${svn_workingcopy_path}
## Getting SVN Revision of Project working copy and updating Project Environment file
SVN_WORKINGCOPY_REVISION=`svn info ${svn_workingcopy_path}  --username ${svn_user} --password ${svn_password}|grep Revision:|awk -F: '{print $2}'|awk -F" " '{print $1}'`

echo "${project_name} local working copy ${svn_workingcopy_path} Revision Number: ${SVN_WORKINGCOPY_REVISION}" |tee -a  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log


PRE_SVN_REVISION=`grep "^previous_svn_revision" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

echo "${project_name} SVN old Revision number:$PRE_SVN_REVISION" >>  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log
/bin/sed -i 's/^'${PRE_SVN_REVISION}'/\previous_svn_revision='${SVN_WORKINGCOPY_REVISION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

echo "Now ${project_name} PREVIOUS SVN REVISION Number in ${project_name}_env.properties file: previous_svn_revision=${SVN_WORKINGCOPY_REVISION}" >> ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

### Getting SVN Revision FROM Repository and updating Project Environment file

FILE_REVISION=`grep "^svn_revision" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

echo "${project_name} SVN LAST Revision number:$FILE_REVISION" >> ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

/bin/sed -i 's/^'${FILE_REVISION}'/\svn_revision='${REVISION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

echo "Now ${project_name}  SVN REVISION Number Number in ${project_name}_env.properties file: svn_revision=${REVISION}" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

sleep 2
:

  fi

else
svn cleanup ${svn_workingcopy_path}


## Getting SVN Revision of Project working copy and updating Project Environment file


SVN_WORKINGCOPY_REVISION=`svn info ${svn_workingcopy_path}  --username ${svn_user} --password ${svn_password}|grep Revision:|awk -F: '{print $2}'|awk -F" " '{print $1}'`

echo "${project_name} local working copy ${svn_workingcopy_path} Revision Number: ${SVN_WORKINGCOPY_REVISION}" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log


PRE_SVN_REVISION=`grep "^previous_svn_revision" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

echo "${project_name} SVN old Revision number:$PRE_SVN_REVISION" |tee -a  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

/bin/sed -i 's/^'${PRE_SVN_REVISION}'/\previous_svn_revision='${SVN_WORKINGCOPY_REVISION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

echo "Now ${project_name} PREVIOUS SVN REVISION Number in ${project_name}_env.properties file: previous_svn_revision=${SVN_WORKINGCOPY_REVISION}" |tee -a  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log


FILE_REVISION=`grep "^svn_revision" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

echo "${project_name} SVN LAST Revision number:$FILE_REVISION" >> ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

/bin/sed -i 's/^'${FILE_REVISION}'/\svn_revision='${REVISION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

echo "Now ${project_name}  SVN REVISION Number Number in ${project_name}_env.properties file: svn_revision=${REVISION}" |tee -a  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

sleep 2
:
fi
echo "$REVISION: 2"

# No Value for Revision number

if [[ ${REVISION} == "" ]];
then

svn cleanup ${svn_workingcopy_path} --username ${svn_user} --password ${svn_password}

## Getting SVN Revision of Project working copy and updating Project Environment file

SVN_WORKINGCOPY_REVISION=`svn info ${svn_workingcopy_path}  --username ${svn_user} --password ${svn_password}|grep Revision:|awk -F: '{print $2}'|awk -F" " '{print $1}'`

echo "${project_name} local working copy ${svn_workingcopy_path} Revision Number: ${SVN_WORKINGCOPY_REVISION}" |tee -a  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log


PRE_SVN_REVISION=`grep "^previous_svn_revision" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

echo "${project_name} SVN old Revision number:$PRE_SVN_REVISION" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

/bin/sed -i 's/^'${PRE_SVN_REVISION}'/\previous_svn_revision='${SVN_WORKINGCOPY_REVISION}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

echo "Now ${project_name} PREVIOUS SVN REVISION Number in ${project_name}_env.properties file: previous_svn_revision=${SVN_WORKINGCOPY_REVISION}" |tee -a   ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

### Getting SVN Revision FROM Repository and updating Project Environment file

SVN_REV=`svn info ${svn_branch} --username ${svn_user} --password ${svn_password}|grep Revision:|awk -F: '{print $2}'|awk -F" " '{print $1}'`

echo "${project_name} SVN Repository Revision Number:${SVN_REV}" | tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log


FILE_REVISION=`grep "^svn_revision" ${play_main_dir}/projects/${project_name}/${project_name}_env.properties`

echo "${project_name} SVN LAST Revision number:$FILE_REVISION" >>  ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log

/bin/sed -i 's/^'${FILE_REVISION}'/\svn_revision='${SVN_REV}'/g' ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

echo "Now ${project_name}  SVN REVISION Number Number in ${project_name}_env.properties file: svn_revision=${SVN_REV}" | tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log
sleep 2
:


else

echo Revision number is $REVISION

fi

echo "Setting up the variable environment file" 

. ${play_main_dir}/projects/${project_name}/${project_name}_env.properties

echo "Crerating tag file"


if [ -d "${svn_change_dir}/${project_name}" ];then

:

else

echo "Creating the Directroy to keep the information ${svn_change_dir}/${project_name}"
mkdir ${svn_change_dir}/${project_name}

fi


if [ -d "${svn_change_dir}/${project_name}/Changes_Made" ];then

:

else

echo "Creating the Directroy to keep the information ${svn_change_dir}/${project_name}/Changes_Made"
mkdir ${svn_change_dir}/${project_name}/Changes_Made

fi

CHANGES_MADE_DIR=${svn_change_dir}/${project_name}/Changes_Made


#keeping history of changes
echo "******** ${project_name} ********"
echo "******** ${project_name} Release Version ${rel_version} following files have been modified or added  between previous SVN revision ${previous_svn_revision} with current Build SVN revision ${svn_revision}" > ${CHANGES_MADE_DIR}/${project_name}_CHANGES_IN_REL_${rel_version}_SVN-REV_${previous_svn_revision}_REV_${svn_revision}_${DATE}.txt

svn log -v -r ${previous_svn_revision}:${svn_revision} ${svn_branch} --username ${svn_user} --password ${svn_password} | tee -a  ${CHANGES_MADE_DIR}/${project_name}_CHANGES_IN_REL_${rel_version}_SVN-REV_${previous_svn_revision}_REV_${svn_revision}_${DATE}.txt

sleep 3
      
cat ${CHANGES_MADE_DIR}/${project_name}_CHANGES_IN_REL_${rel_version}_SVN-REV_${previous_svn_revision}_REV_${svn_revision}_${DATE}.txt | mailx -s "${project_name} Release Version ${rel_version} Modified Changes" "${play_dp_email_addresses}" 

####Removing the SAME Namge tag directory

if [ -d ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version} ]; then

echo "Moving the directory ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version} with date" 

/bin/mv  ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version}   ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version}_${DATE}

else

:

fi 


echo "Updating the working copy of ${project_name} with ${svn_revision}" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log
svn update -r ${svn_revision}  ${svn_workingcopy_path}  --username ${svn_user} --password ${svn_password}


SVN_REV_UPDATE=`svn info ${svn_workingcopy_path}  --username ${svn_user} --password ${svn_password}|grep Revision:|awk -F: '{print $2}'|awk -F" " '{print $1}'`

echo "SVN Working Path:${svn_workingcopy_path}"
echo "SVN Revison number of working copy afer update: ${SVN_REV_UPDATE}";

sleep 3



if [ -d ${tag_prj_dir}/${project_name} ];
then

:

else

echo "Directory: ${tag_prj_dir}/${project_name} does not exist creating it"

/bin/mkdir ${tag_prj_dir}/${project_name}


fi



if [ -d ${tag_prj_dir}/${project_name}/tags ];
then

echo " Copying the ${svn_workingcopy_path} to  ${tag_prj_dir}/${project_name}/tags/ and Release version is ${rel_version}"

/bin/cp  -r ${svn_workingcopy_path} ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version}  

else

/bin/mkdir ${tag_prj_dir}/${project_name}/tags

echo " Copying the ${svn_workingcopy_path} to  ${tag_prj_dir}/${project_name}/tags/ and Release version is ${rel_version}"

/bin/cp  -r ${svn_workingcopy_path} ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version}

fi 





if [ "$?" -eq 0 ];
then
:
else 
echo "Not able to copy ${svn_revision} from ${svn_workingcopy_path} to tag directory ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version}" |tee -a ${play_main_log_dir}/${project_name}/${project_name}_deploy_${DATE}.log
exit 1
fi 

echo "Removing the .SVN directory from TAG directory"


/bin/find ${tag_prj_dir}/${project_name}/tags/${project_name}_${rel_version} -name ".svn" -exec rm -rf {} \; 2>/dev/null


## Saving changes to Manifest file

if [ -d "${svn_change_dir}/${project_name}/Manifest_Changes" ]; then
:

else

echo "Creating the Direcotry to add build and svn Revision chagnes"

mkdir ${svn_change_dir}/${project_name}/Manifest_Changes

fi

MANIFEST_DIR=${svn_change_dir}/${project_name}/Manifest_Changes



cat ${CHANGES_MADE_DIR}/${project_name}_CHANGES_IN_REL_${rel_version}_SVN-REV_${previous_svn_revision}_REV_${svn_revision}_${DATE}.txt > ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${rel_version}_MANIFEST.txt

SVN_REV=`svn info ${svn_branch} --username ${svn_user} --password ${svn_password}|grep Revision:|awk -F: '{print $2}'|awk -F" " '{print $1}'`


echo "" >> ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${rel_version}_MANIFEST.txt
echo ""  >> ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${rel_version}_MANIFEST.txt

echo "SVN Revision after creating TAG ${SVN_REV}"

echo "svn_revision=${SVN_REV}" >> ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${rel_version}_MANIFEST.txt

echo "Adding Relese number to Manifest file ${rel_version}"

echo "rel_version=${rel_version}" >> ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${rel_version}_MANIFEST.txt


BUILD_DATE=`date +%m%d%y`
echo "Adding the Build date to Manifest file"

echo "build_date=${BUILD_DATE}" >> ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${rel_version}_MANIFEST.txt






echo "Hit Enter, to get Deployment Menu" 

read -s -n 1 key

if [[ ${key} = "" ]];

then

. ${play_dp_scripts_dir}/deploy_conf_setup.sh

fi

