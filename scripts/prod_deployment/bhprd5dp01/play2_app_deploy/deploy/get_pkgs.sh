
function export_nexus_pkg () {

printf "\nPlease Enter Relese Version: "
read REL_VERSION

CHECK_REL_VERSION=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')
REL_VER_VERIFICATION=`echo "$CHECK_REL_VERSION" | grep "\." | sed  's/\.//g' | grep "^[0-9]\{1,5\}$"`

if [ "${REL_VER_VERIFICATION}" == "" ]
then

echo "Release number format is incorrect, it must me like: 2.3.4"
echo "Exiting from the deployment script  in 2 seconds"

exit 1

sleep 2

else

:

fi

printf "\nEnter the Nexus Repository User ID: "

read nexus_user

printf "\nEnter the Nexus Repository Password: "

read -rs nexus_password

printf "\n Verifying the Nexus Repository URL Status"

#NEXUS_URL_STATUS=$(curl -sLkv -w  "%{http_code}\\n" "${nexus_URL}" -o /dev/null)

#if [[ $? -eq "0" ]] && [[ ${NEXUS_URL_STATUS} == "200" ]]  || [[ ${NEXUS_URL_STATUS} == "401" ]]
#then

#printf  "\nNexus Repository URL RUNNING FINE\n "
#:
#else


#echo -e  "ALERT :: FAILED Nexus Repository URL
#${nexus_URL} is not responding with Status "200" OK, please check
#Status is ${NEXUS_URL_STATUS}
#\n"
#exit 1
#fi


if [[ -d ${app_pkg_dir_path}/projects/${project_name}/ ]]
then
:

else
echo "Making the directory ${app_pkg_dir_path}/projects/${project_name}"

mkdir -p ${app_pkg_dir_path}/projects/${project_name}/


fi

if [[ -f ${app_pkg_dir_path}/projects/${project_name}/${project_name}-${REL_VERSION}_prd.zip ]]
then

	mv ${app_pkg_dir_path}/projects/${project_name}/${project_name}-${REL_VERSION}_prd.zip ${app_pkg_dir_path}/projects/${project_name}/${project_name}-${REL_VERSION}_prd.zip_${DATE}


cd ${app_pkg_dir_path}/projects/${project_name}
curl --remote-name -k --user "${nexus_user}:${nexus_password}" ${nexus_URL}/${project_name}-${REL_VERSION}_prd.zip


else 

cd ${app_pkg_dir_path}/projects/${project_name}

curl --remote-name -k --user "${nexus_user}:${nexus_password}" ${nexus_URL}/${project_name}-${REL_VERSION}_prd.zip
fi


export REL_PKG=${app_pkg_dir_path}/projects/${project_name}/${project_name}-${REL_VERSION}_prd.zip

echo "Release Package $REL_PKG"

}

export_nexus_pkg

