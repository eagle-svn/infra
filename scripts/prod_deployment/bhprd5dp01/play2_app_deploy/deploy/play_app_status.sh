#!/bin/sh
## Created By Pavan Kumar
## Purpose of this script is to copy the created release package to web servers
#set -x



usage()
{
cat << EOF
usage: $0 options

This script run with these arguments

OPTIONS:
        Pass the Argument 1 as the property file.
        Pass Argument 2 as \"STATUS\" \"START\" \"STOP\" or \"RESTART\"
EOF
}
DATE=$(date +%F-%H-%M-%S)
PROPERTY_FILE=""
if [[ $# -eq "" ]]

then

usage

exit 1

else
:

fi

PROPERTY_FILE=$1

RUN_FUNCTION=$2

if [[ -f ${PROPERTY_FILE} ]];
then

echo "Propertry file : ${PROPERTY_FILE}"
:

else

echo "Property file does not exist"

echo "${PROPERTY_FILE}"

exit 1
fi

.  ${PROPERTY_FILE}


if [ $? -eq 0 ]; then

:

else

echo "Propertry variable has not been setup, please check"

exit 1

fi
echo "Property file is $PROPERTY_FILE"

PROPERTY_FILE_NAME=$(echo "$1" | awk -F "/" '{print $NF}')

export PLAY_HOST=$(echo "${PROPERTY_FILE}" | awk -F\/ '{print $9}' | awk -F. '{print $1}')



if [[ ${RUN_FUNCTION} == "STATUS" ]] ||  [[ ${RUN_FUNCTION} == "START" ]]  ||  [[ ${RUN_FUNCTION} == "STOP" ]] ||   [[ ${RUN_FUNCTION} == "RESTART" ]]
then
:

else

echo "Argument number 2 must be \"STATUS\" \"START\" \"STOP\" or \"RESTART\", please run the script again"
exit1

fi
DATE=`date +%m%d%y_%H%M%S`


function app_status {




echo ""
echo "Checking the status of ${project_name}  application on  Host: ${PLAY_HOST}"

PROCESS_STATUS=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST}  ps -ef | grep "${play_app_name}" |grep -v grep )
echo ""
echo "Status of PLAY Process: ${PROCESS_STATUS}"
echo ""
if [[ "${PROCESS_STATUS}" != "" ]];
then
echo ""
        echo "Play application ${project_name} is running"
        export APP_STATUS=TRUE
echo ""
else
        echo "Play application  ${project_name} at ${PLAY_HOST} is down"
        export APP_STATUS=FALSE

fi



}


function stop_app {


echo ""
echo "Stopping the play application on  Host: ${PLAY_HOST}"

APP_PID=$(ssh -q ${USER_ID}@${PLAY_HOST} cut -f 1 ${play_app_dir}/RUNNING_PID)
echo ""
echo "Killing the Application which has ${PLAY_HOST} PID $APP_PID"
echo ""
sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play kill -9 ${APP_PID}


echo "Cheking if Porcess still running:"
sleep 3
PROCESS_STATUS=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} ps -ef | grep "${play_app_name}" |grep -v grep)
echo ""
echo "Status of PLAY Process: ${PROCESS_STATUS}"
echo ""
if [[ "${PROCESS_STATUS}" == "" ]];
then
echo ""
        echo "Play application ${PLAY_HOST} is down now"
echo ""
else

        PID=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} ps -ef | grep "${play_app_name}" |grep -v grep | awk ' { print $2 } ')

        sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play /bin/kill -9 ${PID}

        PROCESS_STATUS=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play ps -ef | grep "${play_app_name}" |grep -v grep )

        echo "Status of PLAY Process: ${PROCESS_STATUS}"

        if [[ "${PROCESS_STATUS}" == "" ]];
        then


        echo "Play application ${PLAY_HOST} is down now"

        else
                echo "Application has not been down please stop using the play stop and start menu when deployment complete "

                sleep 3
        fi

fi

}


function start_app {


app_status

if [[ ${APP_STATUS} == "FALSE" ]]
then

echo "Startingg the play application on  Host: ${PLAY_HOST}"
echo ""
echo "${USER_ID}@${PLAY_HOST} nohup ${play_app_dir}/bin/${project_name} -Dconfig.resource=${env_file_name}"
echo ""

sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo su - play  bash -c "'

if [[ -f ${play_app_dir}/RUNNING_PID ]]
then
        echo "Running file is there, need to delete this file to start the application"
        rm -f ${play_app_dir}/RUNNING_PID
else
:
fi

'"
sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play nohup ${play_app_dir}/bin/${project_name} ${JVM_MEMORY_ARGS} -Dconfig.resource=${env_file_name} >/dev/null 2>&1 &

echo ""
echo "Application ${project_name} has started, running the status function"
echo ""

else
        echo
        echo "Application is already running on the server, please bring it down"

fi

}


function restart_app {


echo ""
echo "Stopping the play application on  Host: ${PLAY_HOST}"

APP_PID=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play cut -f 1 ${play_app_dir}/RUNNING_PID)
echo ""
echo "Killing the Application which has ${PLAY_HOST} PID $APP_PID"
echo ""
sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play kill -9 ${APP_PID}


echo "Cheking if Porcess still running:"
sleep 3
PROCESS_STATUS=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} ps -ef | grep "${play_app_name}" |grep -v grep )
echo ""
echo "Status of PLAY Process: ${PROCESS_STATUS}"
echo ""
if [[ "${PROCESS_STATUS}" == "" ]];
then
echo ""
        echo "Play application ${PLAY_HOST} is down now"
echo ""
else

        PID=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} ps -ef | grep "${play_app_name}" |grep -v grep | awk ' { print $2 } ')

        sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play /bin/kill -9 ${PID}

        PROCESS_STATUS=$(sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} ps -ef | grep "${play_app_name}" |grep -v grep)

        echo "Status of PLAY Process: ${PROCESS_STATUS}"

        if [[ "${PROCESS_STATUS}" == "" ]];
        then


        echo "Play application ${PLAY_HOST} is down now"

        else
                echo "Application has not been down please stop using the play stop and start menu when deployment complete "

                sleep 3
        fi
fi
echo ""

echo "Startingg the play application on  Host: ${PLAY_HOST}"
echo ""
echo "${USER_ID}@${PLAY_HOST} nohup ${play_app_dir}/bin/${project_name} ${JVM_MEMORY_ARGS} -Dconfig.resource=${env_file_name}"
echo ""

sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play bash -c "'

if [[ -f ${play_app_dir}/RUNNING_PID ]]
then
        echo "Running file is there, need to delete this file to start the application"
        rm -f ${play_app_dir}/RUNNING_PID
else
:
fi

'"

sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play nohup ${play_app_dir}/bin/${project_name} ${JVM_MEMORY_ARGS} -Dconfig.resource=${env_file_name} >/dev/null 2>&1 &

echo ""
echo "Application ${project_name} has started, run the status function"

app_status


}



if [[ ${RUN_FUNCTION} == "STATUS" ]]
then

        echo "Calling the status function"
        app_status
elif  [[ ${RUN_FUNCTION} == "START" ]]
then

        echo "Calling the start function"
        start_app

elif [[ ${RUN_FUNCTION} == "STOP" ]]
then

        echo "Calling the start function"
        stop_app

elif [[ ${RUN_FUNCTION} == "RESTART" ]]
then


        echo "Calling the restart function"
        restart_app

else

echo "Failed: Script failed, please check"
exit1

fi

