#!/bin/sh
## Created By Pavan Kumar
## Purpose of this script is to copy the created release package to web servers

#set -x

function usage()
{
cat << EOF
usage: $0 options

This script run with these parameters

OPTIONS:
        -p Property file for vaiables
        -d Destination of Application TEST, STG, PROD etc.
        -t TAG Directory (Which TAG Directory need to be deployed)
        -h help message to use this script
EOF
}


PROPERTY_FILE=""
REL_TAG_DIR=""
REPLACE_VAR_SCRIPT="/local/apps/bhsis/play_app_deploy/replace_variable.sh"

REPLACE_VAR_SCRIPT_NAME=$(echo "/local/apps/bhsis/play_app_deploy/replace_variable.sh" | awk -F "/" '{print $NF}')
if [[ $# -eq "" ]] 

then

usage

exit 1

else
:

fi


PROPERTY_FILE=$1
REL_TAG_DIR=$2

.  ${PROPERTY_FILE}


if [ $? -eq 0 ]; then

:

else

echo "You have not pass any arguments to the script"

exit 1

fi
DATE=`date +%m%d%y_%H%M%S`


echo "Property file is $PROPERTY_FILE"

PROPERTY_FILE_NAME=$(echo "$1" | awk -F "/" '{print $NF}')

export PLAY_HOST=$(echo "${PROPERTY_FILE}" | awk -F\/ '{print $9}' | awk -F. '{print $1}')



echo "Release Version Number: $REL_VERSION"

echo "USER_ID $USER_ID"

function stop_play_app () {


echo "Stopping the play application on  Host: ${PLAY_HOST} before changing the link for Play application"


APP_PID=$(sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} cut -f 1 ${play_app_dir}/RUNNING_PID)
echo ""
echo "Killing the Application which has ${PLAY_HOST} PID $APP_PID"
echo ""
 sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo -u play  kill -9 ${APP_PID}


echo "Cheking if Porcess still running:"
sleep 3
PROCESS_STATUS=`sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} ps -ef | grep "${play_app_name}" |grep -v grep `
echo ""
echo "Status of PLAY Process: ${PROCESS_STATUS}"
echo ""
if [[ "${PROCESS_STATUS}" == "" ]];
then
echo ""
        echo "Play application ${PLAY_HOST} is down now"
echo ""
else

        PID=`sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST}  ps -ef | grep "${play_app_name}" |grep -v grep | awk ' { print $2 } '`

        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo -u play  /bin/kill -9 ${PID}

        PROCESS_STATUS=`sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} ps -ef | grep "${play_app_name}" |grep -v grep `

        echo "Status of PLAY Process: ${PROCESS_STATUS}"

        if [[ "${PROCESS_STATUS}" == "" ]];
        then


        echo "Play application ${PLAY_HOST} is down now"

        else
                echo "Application has not been down please stop using the play stop and start menu when deployment complete "

                sleep 3
        fi

fi


echo ""

}

function deploy_play_app () {


        echo "Checking if ${play_deploy_path}/${project_name}-${REL_VERSION} already exist at ${PLAY_HOST}"
 echo ""
        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo -u play ls -d ${play_deploy_path}/${project_name}-${REL_VERSION}

                if [ "$?" -eq 0 ]
                then
                echo "Moving the ${play_deploy_path}/${project_name}-${REL_VERSION}"

                echo ""


                 sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo -u play mv  ${play_deploy_path}/${project_name}-${REL_VERSION} ${play_deploy_path}/${project_name}-${REL_VERSION}_${DATE}


                 sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo -u play mv  ${play_deploy_path}/${project_name}-${REL_VERSION}_prd.zip ${play_deploy_path}/${project_name}-${REL_VERSION}_prd.zip_${DATE}


                else

                :

            fi
echo
echo "Release Package: $REL_PKG"
echo

        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo rm -rf  /tmp/${project_name}-${REL_VERSION}_prd.zip
        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} rm -rf  /tmp/${project_name}-${REL_VERSION}_prd.zip

        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo -u play rm -rf  /tmp/${project_name}-${REL_VERSION}_prd.zip

sshpass -p ${ID_PASS} scp -r  ${REL_PKG} ${USER_ID}@${PLAY_HOST}:/tmp

                if [ $? -eq 0 ]; then

                        :

                else

                        echo "SCP is Failed: ${REL_PKG} ${USER_ID}@${PLAY_HOST}:/tmp "
                        echo "Please check the errors"
                        exit 1

                fi

        sleep 2

        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} << EOF
        sudo chown -R play:play /tmp/${project_name}-${REL_VERSION}_prd.zip
        sudo su - play
        mv /tmp/${project_name}-${REL_VERSION}_prd.zip ${play_deploy_path}
EOF
        echo "Removing old link: ${play_app_dir}"

        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} sudo -u play rm -f ${play_app_dir}

                if [ $? -eq 0 ]; then

                        :

                else

                        echo "Can-not remove the link of ${play_app_dir} from  ${USER_ID}@${PLAY_HOST}"
                        echo "Please check the errors"
                        exit 1

                        fi

        echo "Running the UNZIP for ${project_name}-${REL_VERSION}_prd.zip"
echo ""
        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} << EOF
	sudo su - play
        cd ${play_deploy_path}
        unzip ${project_name}-${REL_VERSION}_prd.zip

        ln -s ${play_deploy_path}/${project_name}-${REL_VERSION} ${play_app_dir}
exit
EOF

        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} << EOF

	if [[ -f /tmp/${PROPERTY_FILE_NAME} ]]
	then        
		sudo su - play
		echo "Deleting the /tmp/${PROPERTY_FILE_NAME} "
		rm -f /tmp/${PROPERTY_FILE_NAME}

	fi

        if  [[ -f /tmp/${REPLACE_VAR_SCRIPT_NAME} ]]
        then
                sudo su - play
                echo "Deleting the  /tmp/${REPLACE_VAR_SCRIPT_NAME} file"
                rm -f /tmp/${REPLACE_VAR_SCRIPT_NAME}

        fi


	
EOF


        sshpass -p ${ID_PASS} scp  ${PROPERTY_FILE}  ${REPLACE_VAR_SCRIPT} ${USER_ID}@${PLAY_HOST}:/tmp
        sshpass -p ${ID_PASS} ssh -q ${USER_ID}@${PLAY_HOST} << EOF
       sudo chown -R play:play /tmp/${PROPERTY_FILE_NAME} /tmp/${REPLACE_VAR_SCRIPT_NAME}
	sudo su - play        
        mv /tmp/${REPLACE_VAR_SCRIPT_NAME} ${play_deploy_path}/${project_name}-${REL_VERSION}
	mv /tmp/${PROPERTY_FILE_NAME} ${play_deploy_path}/${project_name}-${REL_VERSION}
	chmod 700 ${play_deploy_path}/${project_name}-${REL_VERSION}/${REPLACE_VAR_SCRIPT_NAME}
	sh ${play_deploy_path}/${project_name}-${REL_VERSION}/${REPLACE_VAR_SCRIPT_NAME} -p ${play_deploy_path}/${project_name}-${REL_VERSION}/${PROPERTY_FILE_NAME} -d ${play_deploy_path}/${project_name}-${REL_VERSION}

EOF


}

function start_play_app () {


echo "Startingg the play application on  Host: ${PLAY_HOST}"
echo ""

echo "${play_id}@${play_host} nohup ${play_deploy_path}/${project_name}-${REL_VERSION}/bin/${project_name} ${JVM_MEMORY_ARGS} -Dconfig.resource=${env_file_name}"

echo ""

sshpass -p ${ID_PASS} ssh -q  ${USER_ID}@${PLAY_HOST} sudo -u play  nohup ${play_deploy_path}/${project_name}-${REL_VERSION}/bin/${project_name} ${JVM_MEMORY_ARGS} -Dconfig.resource=${env_file_name} >/dev/null 2>&1 &

echo ""
echo "Deployment is completed for ${project_name}-${REL_VERSION} at ${play_host}"
echo ""


}
stop_play_app
deploy_play_app
start_play_app
