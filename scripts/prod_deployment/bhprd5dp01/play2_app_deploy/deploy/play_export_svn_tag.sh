
function export_svn_tag () {

printf "\nPlease Enter Relese Version: "
read REL_VERSION

CHECK_REL_VERSION=$(echo ${REL_VERSION} | awk  -F_b '{ print $1 }')
REL_VER_VERIFICATION=`echo "$CHECK_REL_VERSION" | grep "\." | sed  's/\.//g' | grep "^[0-9]\{1,5\}$"`

if [ "${REL_VER_VERIFICATION}" == "" ]
then

echo "Release number format is incorrect, it must me like: 2.3.4"
echo "Exiting from the deployment script  in 2 seconds"

exit 1

sleep 2

else

:

fi

printf "\nEnter the SVN USER ID: "

read svn_user

printf "\nEnter the SVN USER ID Password: "

read -rs svn_password

printf "\nPlease Enter SVN Revision number to copy  TAG(Hit Enter if you want to use Head Revision): "
read  REVISION
echo "Revision number is: $REVISION"

if [[ "${REVISION}" == "" ]]
then
      export EXPORT_REVISION="HEAD"

else
        expr ${REVISION} + 1

        if [[ "$?" -ne 0 ]]
        then

                export  EXPORT_REVISION=${REVISION}
        else

                echo "Revision number has not been enter correctly"

                exit 1

        fi

fi


svn list -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} --username ${svn_user} --password ${svn_password} --no-auth-cache

if [[ "$?" -eq 0 ]]
then
        :

else

echo "Failed: Tag ${svn_export_url}/${project_name}_${REL_VERSION} does not exist on SVN, please check"

exit 1

fi
## SVN export of the release number:

if [[ -d ${play_tag_dir}/projects/${project_name}/tags/${project_name}_${REL_VERSION} ]]
then

	mv ${play_tag_dir}/projects/${project_name}/tags/${project_name}_${REL_VERSION} ${play_tag_dir}/projects/${project_name}/tags/${project_name}_${REL_VERSION}_${DATE}

svn export -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} ${play_tag_dir}/projects/${project_name}/tags/${project_name}_${REL_VERSION}  --username ${svn_user} --password ${svn_password} --no-auth-cache



else 

	svn export -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} ${play_tag_dir}/projects/${project_name}/tags/${project_name}_${REL_VERSION}  --username ${svn_user} --password ${svn_password} --no-auth-cache

fi


if [[ "$?" -eq 0 ]]
then
        :

else

echo "Failed: export command 'svn export -r ${EXPORT_REVISION} ${svn_export_url}/${project_name}_${REL_VERSION} ${play_tag_dir}/projects/${project_name}/tags/${project_name}_${REL_VERSION}' has failed please check"

exit 1

fi


export REL_TAG_DIR=${play_tag_dir}/projects/${project_name}/tags/${project_name}_${REL_VERSION}

echo "Rlease Tage Dir $REL_TAG_DIR"

}

export_svn_tag

