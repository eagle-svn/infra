#!/bin/sh
 
 

USER_ID=`whoami`

if [[ "${USER_ID}" == "scm" ]]
then
:

else
echo "Please login as scm ID to run start and stop Menu"
exit 0

fi

echo "Property file $1"

echo "not calling this menu1"

if [[ $1 == "" ]]
then 

echo "Please pass the property file as an arugment"

exit 1

else
:

fi 

. $1

function play_startstop_menu () {
  case $yourch in
    1)

echo "Stoping the play application on  Host: ${play_host}"

ssh ${play_id}@${play_host} ${play_bin} stop ${play_app_dir}


echo "Cheking if Porcess still running:"

PROCESS_STATUS=`ssh  ${play_id}@${play_host} ps -ef | grep "${play_app_name}" |grep -v grep `

echo "Status of PLAY Process: ${PROCESS_STATUS}"

if [[ "${PROCESS_STATUS}" == "" ]];
then



echo "Play application ${play_host} is down now"
 

echo "Hit Enter to Continue:"

read -s -n 1 key


menu_call

else 


echo "Do you want to kill the process by kill -9 commmand,Enter Y, y or N, n: "

read stopans;

if [[ "${stopans}" == "Y" ]] || [[ "${stopans}" == "y" ]] 

then 

PID=`ssh  ${play_id}@${play_host}  ps -ef | grep "${play_app_name}" |grep -v grep | awk ' { print $2 } '`

ssh ${play_id}@${play_host} /bin/kill -9 ${PID} 

PROCESS_STATUS=`ssh  ${play_id}@${play_host} ps -ef | grep "${play_app_name}" |grep -v grep `

echo "Status of PLAY Process: ${PROCESS_STATUS}"

if [[ "${PROCESS_STATUS}" == "" ]];
then


echo "Play application ${play_host} is down now"


echo "Hit Enter to Continue:"

read -s -n 1 key

menu_call

fi

elif [[ "${stopans}" == "N" ]] || [[ "${stopans}" == "n" ]]

then

echo "Hit Enter to Continue:"

read -s -n 1 key


menu_call

else

echo "Calling Menu again"

echo "Hit Enter to Continue:"

read -s -n 1 key


menu_call

fi 

fi

;;
    2)

echo "Starting the Play application : Host: ${play_host}"

echo " Running the Dependencies command for play" 
ssh ${play_id}@${play_host} << EOF

cd ${play_app_dir}

${play_bin} clean ${play_app_dir}

${play_bin} dependencies --sync

${play_bin} start ${play_app_dir}

EOF

sleep 3


#echo "Now starting the play applcation" 

#ssh ${play_id}@${play_host} ${play_bin} start ${play_app_dir}


if [[ ${project_name} == "HSES" ]]

then

echo "Running the HSES curl command for URL ${app_url}  sleeping for 30 seconds"
sleep 30 
curl ${app_url} >/dev/null

else
:

fi 

echo " "

echo "Hit Enter to Continue:"

read -s -n 1 key


menu_call


;;




    0) exit 0
       ;;
    *)

   menu_call
       ;;
  esac


}


function menu_call () {
  tput clear
  tput setf 5
  printf "* * * * * * * ${play_host} Play Start & Stop Menu * * * * * * *
        ----------------------------------------------
        [1] Play_Stop
        [2] Play_Start
        [0] Exit/stop
        ----------------------------------------------\n"
  tput sgr0
  tput setf 2
  printf   "Enter your menu choice [1-6]:"
  tput sgr0
  read yourch
        export yourch

play_startstop_menu;
        }

menu_call;

