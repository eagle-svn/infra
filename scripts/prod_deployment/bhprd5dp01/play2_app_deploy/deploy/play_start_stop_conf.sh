#!/bin/sh
### Created by Pavan Kumar



function play_environment_menu () {

tput clear
tput setf 5
printf "* * * * * * * ${project_name} Play Start and Stop Menu * * * * * * *
        --------------------------------------------------
        [1] DEV1A
        [2] DEV2A
        [3] TEST1A
        [4] TEST2A
        [5] PROD1A
        [6] PROD2A
	[7] BHS2DEV1A
        [0] EXIT From this Menu, Called Main Menu Again
        ----------------------------------------------\n"
tput sgr0
tput setf 2
  printf "Enter your menu choice [1-5]:"

tput sgr0
  read yourch
  case $yourch in
    1)

   APP_ENV=DEV1A

. ${play_dp_scripts_dir}/play_startStop.sh ${play_main_dir}/projects/${project_name}/play_${project_name}_${APP_ENV}.properties

;;



   2)

   APP_ENV=DEV2A

. ${play_dp_scripts_dir}/play_startStop.sh ${play_main_dir}/projects/${project_name}/play_${project_name}_${APP_ENV}.properties



;;

   3)

   APP_ENV=TEST1A

. ${play_dp_scripts_dir}/play_startStop.sh ${play_main_dir}/projects/${project_name}/play_${project_name}_${APP_ENV}.properties

                
;;


   4)

   APP_ENV=TEST2A

. ${play_dp_scripts_dir}/play_startStop.sh ${play_main_dir}/projects/${project_name}/play_${project_name}_${APP_ENV}.properties


;;




   5)

	tput setf 4
	printf "You have select ${project_name} PRODUCTION Play  to  start and stop\n"
	sleep 2
	printf "Are you sure you want to do start and stop Production Play, if yes type YES in capital to continue:"
	tput sgr0
read prdans

if [[ ${prdans} == "YES" ]];then

printf "You have selected production deployment\n"
sleep 2
:

else
tput setf 4
printf "Kicking you out of the script, please run menu again:\n"
tput sgr0
exit 1

fi


   APP_ENV=PROD1A

. ${play_dp_scripts_dir}/play_startStop.sh ${play_main_dir}/projects/${project_name}/play_${project_name}_${APP_ENV}.properties


;;






   6)

       tput setf 4
        printf "You have select ${project_name} PRODUCTION Play  to  start and stop\n"
        sleep 2
        printf "Are you sure you want to do start and stop Production Play, if yes type YES in capital to continue:"
        tput sgr0
read prdans

if [[ ${prdans} == "YES" ]];then

printf "You have selected production deployment\n"
sleep 2
:

else
tput setf 4
printf "Kicking you out of the script, please run menu again:\n"
tput sgr0
exit 1

fi


   APP_ENV=PROD2A

. ${play_dp_scripts_dir}/play_startStop.sh ${play_main_dir}/projects/${project_name}/play_${project_name}_${APP_ENV}.properties


;;


 7)

   APP_ENV=BHS2DEV1A

. ${play_dp_scripts_dir}/play_startStop.sh ${play_main_dir}/projects/${project_name}/play_${project_name}_${APP_ENV}.properties


;;



   0)
. /local/apps/bhsis/play_app_deploy/main_menu.sh
       ;;
   *)

   exit 0
       ;;
   esac

}



play_environment_menu



