#!/bin/sh
## Created By Pavan Kumar
## Purpose of this script is to copy the created release package to web servers




usage()
{
cat << EOF
usage: $0 options

This script run with these parameters

OPTIONS:
        -p Property file for vaiables
        -d Destination of Application TEST, STG, PROD etc.
        -t TAG Directory (Which TAG Directory need to be deployed)
        -h help message to use this script
EOF
}

PROPERTY_FILE=""
REL_TAG_DIR=""


if [[ $# -eq "" ]] 

then

usage

exit 1

else
:

fi

while getopts "hp:r:" OPTION

do

case $OPTION in
        h) usage
                exit 1 ;;

        p) PROPERTY_FILE=$OPTARG
        ;;

        r) REL_TAG_DIR=$OPTARG
        ;;

        ?)
        usage
        exit
        ;;
esac

done


if [[ -f ${PROPERTY_FILE} ]]; 
then

echo "Propertry file : ${PROPERTY_FILE}"
:

else

echo "Property file does not exist"

echo "${PROPERTY_FILE}"

exit 1
fi

if [[ -d ${REL_TAG_DIR} ]] ;
then


:

else
echo "TAG Directory does not exist: ${REL_TAG_DIR}"

exit 1

fi


.  ${PROPERTY_FILE}


if [ $? -eq 0 ]; then

:

else

echo "Propertry variable has not been setup, please check"

exit 1

fi
DATE=`date +%m%d%y_%H%M%S`


echo "Adding Manifest file to Release TAG DIR : ${project_name}_${APP_DEPLOY_VER}"

## Adding the Date in Manifest file
DEPLOY_DATE=`date +%m%d%y`

echo "deploy_date=${DEPLOY_DATE}" >> ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt



### Adding TAG to SVN when deploying to PROD. 



if [[ "${app_env}" == "PROD" ]]

then 

	printf "Do you want to add release tag directory to SVN?, Enter YES if you want to:"

	read value;

	if [[ "${value}" == "YES" ]] 
	then

                printf "Enter your SVN User ID:"
                read SVN_USER_NAME;

                printf "Enter your SVN User Password, you won't see the passoword on screen:"
                read -rs SVN_USER_PASS;

                echo "Checking if you can access the SVN with your SVN User ID and Password"

                svn info ${svn_URL} --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache --non-interactive

                if [[ $? -eq "0" ]];then
                        :
                else
                        echo "You have entered the wrong SVN User ID and Password, Rerun the deployment again"

                        exit 1
                fi

                printf "Enter the Production deployment JIRA ticket number - (dash) and JIRA ticket summary that you put in JIRA, for example: OPS-000 - JIRA_SUMMARY:"
                read JIRA_TICKET;

                if [[ $JIRA_TICKET == "" ]]
                then
                        echo "Please enter the information of deplyment JIRA tiecket, exiting from the script"

                        exit 1
                fi


		
 	        echo "Getting the SVN Revison number of the tag"
                TAG_SVN_REVISION=$(grep "svn_revision" ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt | awk -F= '{print $2}')

                echo "SVN TAG Revison nubmer for the ${APP_DEPLOY_VER} is $TAG_SVN_REVISION"

		echo "Checking if ${svn_tag_dir} exist or not"

        		if [[ ! -d "${svn_tag_dir}" ]]

        		then

        			echo "Directory does not exist in svn working copy, making the directory and saving it to svn"


        			svn mkdir ${svn_tag_dir} --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache

                		if [ $? -eq 0 ]; then


                			svn ci ${svn_tag_dir} -m "added ${svn_tag_dir} to svn" --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache

                		else

					echo "Not able to add svn directory in SVN"

                		exit 1

                		fi


        		else

        			echo "doing svn update on ${svn_tag_dir}"

        			svn update ${svn_tag_dir} --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache


        		fi



		echo "Verifying the tag directory exist or not in svn tags if exist taking bakcup of it and adding release tag to svn"


        		if [[ -d "${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER}" ]]

        		then

        			svn move ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER} ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER}_${DATE} --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache

				svn ci ${svn_tag_dir} -m "Moved ${project_name}_${APP_DEPLOY_VER} to ${project_name}_${APP_DEPLOY_VER}_${DATE}" --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache


                                echo "svn copy -r${TAG_SVN_REVISION} ${svn_URL} ${svn_tag_url}/${project_name}_${APP_DEPLOY_VER}"
                                svn copy -r${TAG_SVN_REVISION} ${svn_URL}/ ${svn_tag_url}/${project_name}_${APP_DEPLOY_VER} -m "${JIRA_TICKET} - added ${project_name}_${APP_DEPLOY_VER} tag in to SVN" --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache

                                svn update ${svn_tag_dir} --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache


                                cp ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER}

                                svn add ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER}/${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache

                                svn ci ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER} -m "${JIRA_TICKET} - adding the ${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt to ${project_name}_${APP_DEPLOY_VER} tags" --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache


        		else


                                echo "svn copy -r${TAG_SVN_REVISION} ${svn_URL} ${svn_tag_url}/${project_name}_${APP_DEPLOY_VER}"
                                svn copy -r${TAG_SVN_REVISION} ${svn_URL}/ ${svn_tag_url}/${project_name}_${APP_DEPLOY_VER} -m "${JIRA_TICKET} - added ${project_name}_${APP_DEPLOY_VER} tag in to SVN" --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache 

                                svn update ${svn_tag_dir} --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache


                                cp ${svn_change_dir}/${project_name}/Manifest_Changes/${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER}

                                svn add ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER}/${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache

                                svn ci ${svn_tag_dir}/${project_name}_${APP_DEPLOY_VER} -m "${JIRA_TICKET} - adding the ${project_name}_REL_${APP_DEPLOY_VER}_MANIFEST.txt to ${project_name}_${APP_DEPLOY_VER} tags" --username ${SVN_USER_NAME} --password ${SVN_USER_PASS} --no-auth-cache


      			fi


	else

	echo "Not adding this release tag directory to SVN"

	fi

 	

fi



echo "Hit Enter to go back for Menu "
read -s -n 1 key


