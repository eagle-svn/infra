#!/bin/sh

DATE=`date +%m%d%y_%H%M%S`
ply_main_dp_log="/local/apps/bhsis/play2_app_deploy/logs/play_main_db_${DATE}.log"

function case_menu () {
 case $yourch in
    1) 

printf "Setting the Environmnet Variable for Locator Project\n"  | tee -a ${ply_main_dp_log} 

. /local/apps/bhsis/play2_app_deploy/projects/locator_play2_app/locator_play2_app_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else 
 tput setf 4
 printf "Failed:: Environment Variable has not been set for Locator project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1 

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;

   2)

printf "Setting the Environmnet Variable for CTS Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/CTS/CTS_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for CTS project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;

  3)

printf "Setting the Environmnet Variable for BRC Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/brc/brc_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for HSES project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;


  4)

printf "Setting the Environmnet Variable for CRON_CTS Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/CRON_CTS/CRON_CTS_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for CRON_CTS project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;



  5)

printf "Setting the Environmnet Variable for IBHS Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/ibhs/ibhs_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for IBHS project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;

  6)

printf "Setting the Environmnet Variable for DSS Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/dss/dss_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for DSS project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;

  7)

printf "Setting the Environmnet Variable for AIMS Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/aims/aims_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for AIMS project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;


  8)

printf "Setting the Environmnet Variable for dlt Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/dlt/dlt_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for dlt project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;


  9)

printf "Setting the Environmnet Variable for BIMS Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/BIMS/BIMS_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for BIMS project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;


 10)

printf "Setting the Environmnet Variable for Auth Service Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/auth_service/auth_service_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for auth_service project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;

 11)

printf "Setting the Environmnet Variable for Data Extract Project\n"  | tee -a ${ply_main_dp_log}

. /local/apps/bhsis/play2_app_deploy/projects/data-extract/data-extract_env.properties

 if [ "$?" -eq 0 ];
   then
        :
 else
 tput setf 4
 printf "Failed:: Environment Variable has not been set for data-extract project\n" |tee -a ${ply_main_dp_log}
 tput sgr0
exit 1

 fi

. /local/apps/bhsis/play2_app_deploy/play_options_menu.sh

;;





    0) exit 0
       ;;
    *)

    exit 0
       ;;
  esac

}


function main_menu () {
  tput clear
  tput setf 5
  printf "* * * * * * * DASIS Build and Deploy MENU * * * * * * *
        ----------------------------------------------
        [1] locator
        [0] Exit/stop
        ----------------------------------------------\n"


#  printf "* * * * * * * DASIS Build and Deploy MENU * * * * * * *
#        ----------------------------------------------
#        [1] locator_play2_app
#        [2] CTS
#	[3] brc
#        [4] CRON_CTS
#        [5] ibhs
#	[6] dss
#	[7] aims
#	[8] dlt
#	[9] BIMS
#	[10] auth_service
#	[11] data-extract
#        [0] Exit/stop
#        ----------------------------------------------\n"
  tput sgr0
  tput setf 2
  printf   "Enter your menu choice [1-5]:"
  tput sgr0
  read yourch

 export yourch
case_menu
}

main_menu



